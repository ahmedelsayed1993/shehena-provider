package com.aait.shehenaprovider.Listners;

/**
 * Created by mahmoud on 12/03/2017.
 */

public interface NavigationActionListner {

    void onProfile();

    void onNotification();

    void onAccounts();

    void onRequestes();
    void onNewRequestes();
    void onFinishRequestes();

    void onSettings();

    void onAbout();

    void onShare();

    void onLogout();

}
