package com.aait.shehenaprovider.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Activities.SplashActivity;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

/**
 * Created by Amrel on 09/04/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {





    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("notifyyyyyy",new Gson().toJson(remoteMessage.getData()));
        if (remoteMessage.getData() != null) {
            GlobalPreferences globalPreferences = new GlobalPreferences(getApplicationContext());
            Intent myIntent;

            if (globalPreferences.getLoginStatus()) {
                myIntent = new Intent(this, MainActivity.class);
                myIntent.putExtra("state", true);
            } else {
                myIntent = new Intent(this, SplashActivity.class);
            }

            showNotification(remoteMessage,myIntent);

        }
    }
    private void showNotification(RemoteMessage message, Intent intent) {


        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("body"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.logo);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }

    private void showNotification(RemoteMessage message) {
        GlobalPreferences globalPreferences = new GlobalPreferences(getApplicationContext());
        Intent myIntent;

        if (globalPreferences.getLoginStatus()) {
            myIntent = new Intent(this, MainActivity.class);
            myIntent.putExtra("state", true);
        } else {
            myIntent = new Intent(this, SplashActivity.class);
        }
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                myIntent,
                PendingIntent.FLAG_ONE_SHOT);

        Notification myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("body"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.loger)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, myNotification);
    }

}



