package com.aait.shehenaprovider.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Adapter.OrdersAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.NewOrderModel.NewOrderData;
import com.aait.shehenaprovider.Models.NewOrderModel.newOrdersModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by aya on 2/7/2017.
 */

public class NewOrdersFragment extends BaseFragment {

    @BindView(R.id.rv_recycle)
    RecyclerView rv_recycle;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;
    OrdersAdapter adapter;
    private List<NewOrderData> models;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swiperefresh;

    @Override
    protected int getViewId() {
        return R.layout.fragment_main;
    }

    @Override
    protected void init(View view) {
       // toaster.makeToast(globalPreferences.getID());

        models = new ArrayList<>();
        adapter = new OrdersAdapter(getContext(),models);
        rv_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_recycle.setAdapter(adapter);
        getOrders();
        onRefresh();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.new_orders);
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new NewOrdersFragment();
        return frag;
    }

    public void getOrders() {
          CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNewOrders(globalPreferences.getID(),globalPreferences.getLang()).enqueue(new Callback<newOrdersModel>() {
            @Override
            public void onResponse(Call<newOrdersModel> call, Response<newOrdersModel> response) {
                swiperefresh.setRefreshing(false);
                CustomeProgressDialog.onFinish();
                adapter = new OrdersAdapter(getContext(), response.body().getData());
                rv_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rv_recycle.setAdapter(adapter);
                if (response.body().getData().size()==0)
                {
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);


                    Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                    no_data.setText(getString(R.string.no_data));
                }
            }
            @Override
            public void onFailure(Call<newOrdersModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
                swiperefresh.setRefreshing(false);


                Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));

            }

        });
    }
    private void onRefresh() {
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getOrders();
                        notloading.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                    }
                }
        );
        swiperefresh.setColorSchemeResources(R.color.colorPrimary, R.color.orange, R.color.yellow);

    }
}
