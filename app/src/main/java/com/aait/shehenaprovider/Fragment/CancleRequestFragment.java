package com.aait.shehenaprovider.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.RefusedOrderModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class CancleRequestFragment extends BaseFragment {

    @BindView(R.id.btn_sure)
    Button btn_sure;
    @BindView(R.id.sp_cancel)
    Spinner sp_cancel;
    @BindView(R.id.ed_notes)
    EditText ed_notes;
    String order_id;
    String refuserOrder_id;
    ArrayList<String> reason = new ArrayList<>();
    String[] spinnerItemsForCategories;
    String selected_reason="";
    String notes="";
    @Override
    protected int getViewId() {
        return R.layout.fragment_cancel_request;
    }

    @Override
    protected void init(View view) {
        Bundle bundle=getArguments();
        order_id=bundle.getString("order_id");
        getReasons();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.cancel_request);
    }

    @Override
    public void onClick(View view) {

    }
    private boolean validateNotes() {
        if (ed_notes.getText().toString().trim().isEmpty()) {
            ed_notes.setError(getString(R.string.enter_notes));
            Util.requestFocus(ed_notes, getActivity().getWindow());
            return false;
        }
        return true;
    }
    @OnClick(R.id.btn_sure)
    void Sure()
    {
        if (validateNotes()) {
            getAddRefused();
            Log.e("data", globalPreferences.getID() + "  " + order_id + "  " + selected_reason + "  " + ed_notes.getText().toString());
        }
    }
    public void getAddRefused() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRefusedOrder(globalPreferences.getID(),
                order_id,selected_reason,ed_notes.getText().toString()).enqueue(new Callback<RefusedOrderModel>() {
            @Override
            public void onResponse(Call<RefusedOrderModel> call, Response<RefusedOrderModel> response) {
                Util.onPrintLog(response.body());
                if (response.body().getStatus().equals("1")) {
                    // toaster.makeToast(response.body().getMessage());
                    //refuserOrder_id=response.body().getRefusedOrder_id();
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CancelDoneFragment.class.getName()).replace(R.id.content, new CancelDoneFragment()).commit();
                }else {
                    toaster.makeToast(getString(R.string.clientCancel));
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();
               }
                CustomeProgressDialog.onFinish();
            }

            @Override
            public void onFailure(Call<RefusedOrderModel> call, Throwable t) {

            }

        });
    }

    public void SetSpinnerReason() {
        ArrayAdapter<CharSequence> category_adapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForCategories
        );
        sp_cancel.setAdapter(category_adapter);
    }
    public void getReasons() {
        List<String> reasons = new ArrayList<String>();
        reasons.add(getString(R.string.reason1));
        reasons.add(getString(R.string.reason2));
        reasons.add(getString(R.string.reason3));

        for (int i = 0; i < reasons.size(); i++) {
                        reason.add(reasons.get(i).toString());
                    }
                    spinnerItemsForCategories = new String[reason.size()];
                    spinnerItemsForCategories = reason.toArray(spinnerItemsForCategories);
                    SetSpinnerReason();

        sp_cancel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_reason=sp_cancel.getSelectedItem().toString();
                // id_category=arrayList.get(position).getId();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

    }

}
