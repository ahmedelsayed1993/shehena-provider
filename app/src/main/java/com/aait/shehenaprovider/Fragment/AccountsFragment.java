package com.aait.shehenaprovider.Fragment;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Adapter.AccountsAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.BankModel.bankModel;
import com.aait.shehenaprovider.Models.PostCommissionModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by aya on 2/7/2017.
 */

public class AccountsFragment extends BaseFragment {

    @BindView(R.id.rl_banks)
    RecyclerView rl_banks;
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.ed_account_number_from)
    EditText ed_account_number_from;
    @BindView(R.id.ed_account_number_to)
    EditText ed_account_number_to;
    @BindView(R.id.ed_account_owner)
    EditText ed_account_owner;
    @BindView(R.id.ed_money)
    EditText ed_money;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;
    AccountsAdapter adapter;

    @Override
    protected int getViewId() {
        return R.layout.fragment_accounts;
    }

    @Override
    protected void init(View view) {
        getBanks();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.transaction);
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new AccountsFragment();
        return frag;
    }
    @OnClick(R.id.btn_send)
    void send() {
        if (validationAccounts()) {
            getCommission();

        }
    }
    private boolean validationAccounts() {
        if (!validateAccountFrom()) {
            return false;
        }
        if (!validateAccountTo()) {
            return false;
        }
        if (!validateAccountOwner()) {
            return false;
        }

        if (!validateMoney()) {
            return false;
        }
        return true;
    }

    private boolean validateAccountFrom() {
        if (ed_account_number_from.getText().toString().trim().isEmpty()) {
            ed_account_number_from.setError(getString(R.string.enter_account_from));
            Util.requestFocus(ed_account_number_from, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validateAccountTo() {
        if (ed_account_number_to.getText().toString().trim().isEmpty()) {
            ed_account_number_to.setError(getString(R.string.enter_account_to));
            Util.requestFocus(ed_account_number_to, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateAccountOwner() {
        if (ed_account_owner.getText().toString().trim().isEmpty()) {
            ed_account_owner.setError(getString(R.string.enter_accunt_owner));
            Util.requestFocus(ed_account_owner, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validateMoney() {
        if (ed_money.getText().toString().trim().isEmpty()) {
            ed_money.setError(getString(R.string.enter_money));
            Util.requestFocus(ed_money, getActivity().getWindow());
            return false;
        }
        return true;
    }
    public void getBanks() {
          CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getBanks(globalPreferences.getLang(),globalPreferences.getID()).enqueue(new Callback<bankModel>() {
            @Override
            public void onResponse(Call<bankModel> call, Response<bankModel> response) {
                customeProgressDialog.onFinish();
                Util.onPrintLog(response.body().getData().getCommission());
                Log.e("jjhkjhkhk",response.body().getData().getCommission()+"");
                tv_total.setText(response.body().getData().getCommission()+getString(R.string.coin));
                adapter = new AccountsAdapter(getContext(), response.body().getData().getBanks());
                rl_banks.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rl_banks.setAdapter(adapter);
                if (response.body().getData().getBanks().size()==0)
                {
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);

                    Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                    no_data.setText(getString(R.string.no_data));
                }
            }

            @Override
            public void onFailure(Call<bankModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);

                Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));

            }

        });
    }
    public void getCommission() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCommission(globalPreferences.getID(),ed_account_number_from.getText().toString(),
                ed_account_number_to.getText().toString(),ed_money.getText().toString(),ed_account_owner.getText().toString()
        ,globalPreferences.getLang()).enqueue(new Callback<PostCommissionModel>() {
            @Override
            public void onResponse(Call<PostCommissionModel> call, Response<PostCommissionModel> response) {
                customeProgressDialog.onFinish();
                if (response.body().getStatus().equals("1")) {
                    toaster.makeToast(response.body().getMessage());
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();
                }
                else
                {
                    toaster.makeToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PostCommissionModel> call, Throwable t) {
                customeProgressDialog.onFinish();

            }

        });
    }
}
