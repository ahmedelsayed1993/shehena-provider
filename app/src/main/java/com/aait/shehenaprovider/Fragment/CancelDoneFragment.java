package com.aait.shehenaprovider.Fragment;

import android.view.View;
import android.widget.RelativeLayout;

import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class CancelDoneFragment extends BaseFragment {

    @BindView(R.id.rl_cancel)
    RelativeLayout rl_cancel;

    @Override
    protected int getViewId() {
        return R.layout.fragment_cancel_done;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected String SetTitle() {
        return null;
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.rl_cancel)
    void Cancel()
    {
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();

        //  getActivity().getSupportFragmentManager().popBackStack(CurrentOrdersFragment.class.getName(), 0);

    }


}
