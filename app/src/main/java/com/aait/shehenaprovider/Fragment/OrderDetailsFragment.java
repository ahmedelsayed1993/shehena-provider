package com.aait.shehenaprovider.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Adapter.AssistancesAdapter2;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Components.LitterAsImage;
import com.aait.shehenaprovider.Models.AssistantsModel2;
import com.aait.shehenaprovider.Models.CurrentOrderModel.CurrentData;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.Order_data;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.User_data;
import com.aait.shehenaprovider.Models.Process_array;
import com.aait.shehenaprovider.Models.AcceptOrderModel.acceptOrderModel;

import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class OrderDetailsFragment extends BaseFragment {
    @BindView(R.id.btn_yes)
    Button btn_yes;
    @BindView(R.id.btn_no)
    Button btn_no;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.tv_username)
    TextView tv_username;
    @BindView(R.id.civ_user_img)
    CircleImageView civ_user_img;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_service_type)
    TextView tv_service_type;
    @BindView(R.id.tv_payment_method)
    TextView tv_payment_method;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.tv_start_date)
    TextView tv_start_date;
    @BindView(R.id.tv_request_num)
    TextView tv_request_num;
    @BindView(R.id.tv_state)
    TextView tv_state;
    User_data user_data;
    Order_data order_data;
    CurrentData currentOrder_data;
    String order_id;
    String page;
    DateFormat df = new SimpleDateFormat("d MMM, HH:mm");
    String date = df.format(Calendar.getInstance().getTime());
    String title = "";
    String status;

    String userPhone;

    @BindView(R.id.tv_request_time)
    TextView tv_request_time;

    @BindView(R.id.recycler)
    RecyclerView recycler;
    AssistancesAdapter2 mAdapter;

    @Override
    protected int getViewId() {
        return R.layout.fragment_order_details;
    }

    @Override
    protected void init(View view) {


        Bundle bundle = getArguments();
        page = bundle.getString("page");
        order_id = bundle.getString("id");
        Log.i("Order_id", order_id);
        if (page.equals("new")) {

            title = getString(R.string.new_orders);
            btn_next.setVisibility(View.GONE);
            btn_no.setVisibility(View.VISIBLE);
            btn_yes.setVisibility(View.VISIBLE);
            user_data = (User_data) bundle.getSerializable("user_data");
            order_data = (Order_data) bundle.getSerializable("order_data");
            status = bundle.getString("status");
            tv_username.setText(user_data.getUsername());
            LitterAsImage litterAsImage = new LitterAsImage(getActivity());
            Bitmap bitmap = litterAsImage.litterToImage(R.drawable.oval_colorprimary, user_data.getUsername());
            Drawable d = new BitmapDrawable(getActivity().getResources(), bitmap);

           /* if (!user_data.getAvatar().equals("")) {
                Picasso.with(getActivity()).load(user_data.getAvatar()).placeholder(d).resize(200, 200).into(civ_user_img);

            } else {

                civ_user_img.setImageBitmap(bitmap);
            }*/
            tv_name.setText(user_data.getUsername());
            String payment = order_data.getPayment_type();
            if (payment.equals("cash"))
                tv_payment_method.setText(getString(R.string.cash));
            else
                tv_payment_method.setText(getString(R.string.online));
            tv_price.setText(order_data.getPrice());
            tv_start_date.setText(order_data.getDate());
            tv_request_time.setText(order_data.getTime());
            tv_service_type.setText(order_data.getService_name());
            tv_request_num.setText(" " + order_data.getOrder_num());

            userPhone=order_data.getUser_phone();

            initRecycler(order_data.getAssistant());

            if (status.equals("0"))
                    tv_state.setText(getResources().getString(R.string.the_accept));
                else if (status.equals("1"))
                    tv_state.setText(getResources().getString(R.string.on_away));
                else if (status.equals("2"))
                    tv_state.setText(getResources().getString(R.string.arrival));
                else if (status.equals("3"))
                    tv_state.setText(getResources().getString(R.string.start_work));
                else if (status.equals("4"))
                    tv_state.setText(getResources().getString(R.string.end));
                else if (status.equals("5"))
                    tv_state.setText(getResources().getString(R.string.complete));


        } else if (page.equals("current")) {
            title = getString(R.string.orders);
            btn_next.setVisibility(View.VISIBLE);
            btn_no.setVisibility(View.GONE);
            btn_yes.setVisibility(View.GONE);
            currentOrder_data = (CurrentData) bundle.getSerializable("data");
            tv_username.setText(currentOrder_data.getUser_name());
            LitterAsImage litterAsImage = new LitterAsImage(getActivity());
            Bitmap bitmap = litterAsImage.litterToImage(R.drawable.oval_colorprimary, currentOrder_data.getUser_name());
            Drawable d = new BitmapDrawable(getActivity().getResources(), bitmap);


            tv_name.setText(currentOrder_data.getUser_name());
            tv_payment_method.setText(currentOrder_data.getPayment_type());
            tv_price.setText(currentOrder_data.getPrice());
            tv_start_date.setText(currentOrder_data.getDate());
            tv_service_type.setText(currentOrder_data.getService_name());
            tv_request_num.setText(currentOrder_data.getOrder_num()+"");
            tv_request_time.setText(currentOrder_data.getTime());
            status = String.valueOf(currentOrder_data.getOrder_processing());


            Log.e("current ordrs",new Gson().toJson(currentOrder_data.getAssistants()));

            ArrayList<AssistantsModel2> playersList= (ArrayList<AssistantsModel2>) fromJson(currentOrder_data.getAssistants(),
                    new TypeToken<ArrayList<AssistantsModel2>>() {
                    }.getType());
            Log.e("current ordrs2",new Gson().toJson(playersList));

            initRecycler(playersList);



            if (status.equals("0"))
                tv_state.setText(getResources().getString(R.string.wait));
            else if (status.equals("1"))
                tv_state.setText(getResources().getString(R.string.accept));
            else if (status.equals("2"))
                tv_state.setText(getResources().getString(R.string.on_away));
            else if (status.equals("3"))
                tv_state.setText(getResources().getString(R.string.arrival));
            else if (status.equals("4"))
                tv_state.setText(getResources().getString(R.string.start_work));
            else if (status.equals("5"))
                tv_state.setText(getResources().getString(R.string.end));
            else if (status.equals("6"))
                tv_state.setText(getResources().getString(R.string.complete));

        }
    }

    public static Object fromJson(String jsonString, Type type) {
        return new Gson().fromJson(jsonString, type);
    }




    @Override
    protected String SetTitle() {
        return title;
    }

    @Override
    public void onClick(View view) {

    }


    private void initRecycler(List<AssistantsModel2> assistans){
        mAdapter=new AssistancesAdapter2(getContext());
        for (AssistantsModel2 a : assistans) {
            mAdapter.addAssistance(a);
        }
        recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(mAdapter);
    }
    public void getAcceptNewOrder() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAcceptOrder(order_id).enqueue(new Callback<acceptOrderModel>() {
            @Override
            public void onResponse(Call<acceptOrderModel> call, final Response<acceptOrderModel> response) {
                Util.onPrintLog(response.body());
                customeProgressDialog.onFinish();
                if (response.body() != null) {
                    if (response.body().getProcess_array()!=null) {






                        final Dialog dialog;

                        dialog=new Dialog(getActivity(),android.R.style.Theme_Black_NoTitleBar);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                        dialog.setContentView(R.layout.card_dialog);
                        dialog.setCancelable(true);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();

                        Button next =(Button) dialog.findViewById(R.id.next);
                        Button callButton =(Button) dialog.findViewById(R.id.call);
                        TextView text =  (TextView) dialog.findViewById(R.id.phone);
                        text.setText(userPhone);

                        callButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("yy",userPhone);
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:"+userPhone));
                                OrderDetailsFragment.this.startActivity(intent);
                            }
                        });
                        next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                dialog.cancel();
                                List<Process_array> process_arrays = response.body().getProcess_array();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("array", (Serializable) process_arrays);
                                bundle.putString("order_id", order_id);
                                bundle.putString("page", "new");
                                bundle.putString("order_num", order_data.getOrder_num()+"");
                                FragmentOrderNumberFragment fragment = new FragmentOrderNumberFragment();
                                fragment.setArguments(bundle);
                                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(FragmentOrderNumberFragment.class.getName()).replace(R.id.content, fragment).commit();
                            }
                        });

                    } else {
                        toaster.makeToast(response.body().getMessage());
                    }
                } else {
                    Log.i("NoData", "Body is null OrderDetails getAcceptOrder");
                }

                CustomeProgressDialog.onFinish();
            }

            @Override
            public void onFailure(Call<acceptOrderModel> call, Throwable t) {

            }

        });
    }

    public void getNextCurrentOrder() {

        Bundle bundle = new Bundle();
        bundle.putSerializable("array", (Serializable) currentOrder_data.getProcess_array());
        bundle.putString("order_id", order_id);
        bundle.putString("page", "current");
        bundle.putString("order_num", currentOrder_data.getOrder_num()+"");
        Log.e("gggg", order_id + "    " + currentOrder_data.getOrder_num() + "  " + currentOrder_data.getProcess_array());
        FragmentOrderNumberFragment fragment = new FragmentOrderNumberFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(FragmentOrderNumberFragment.class.getName()).replace(R.id.content, fragment).commit();
    }

    @OnClick(R.id.btn_yes)
    void Yes()

    {
        getAcceptNewOrder();
    }

    @OnClick(R.id.btn_next)
    void Next()

    {
        getNextCurrentOrder();
    }

    @OnClick(R.id.btn_no)
    void No() {
        Bundle bundle = new Bundle();
        bundle.putString("order_id", order_id);
        CancleRequestFragment fragment = new CancleRequestFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CancleRequestFragment.class.getName()).replace(R.id.content, fragment).commit();
    }
}
