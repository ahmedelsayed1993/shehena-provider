package com.aait.shehenaprovider.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Adapter.NotificationAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Listners.INotificationFragment;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModel;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModelData;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by aya on 2/7/2017.
 */

public class NotificationFragment extends BaseFragment implements INotificationFragment{

    @BindView(R.id.rv_notification)
    RecyclerView rv_notification;
    NotificationAdapter adapter;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout activity_main_swipe_refresh_layout;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;

    @Override
    protected int getViewId() {
        return R.layout.fragment_notification;
    }

    @Override
    protected void init(View view) {
        activity_main_swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Util.isNetworkAvailable(getActivity())) {
                    activity_main_swipe_refresh_layout.setRefreshing(false);
                  //  Log.e(TAG, "Internet connection is available");
                } else {
                    activity_main_swipe_refresh_layout.setRefreshing(false);
                   // Log.e(TAG, "no Available Internit connection");
                }
            }
        });
        activity_main_swipe_refresh_layout.setColorSchemeResources(R.color.black, R.color.white, R.color.colorPrimary);
        adapter = new NotificationAdapter(getContext(),NotificationFragment.this);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.clearNotifications();
        getNotification();
    }

    public void getNotification(){
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        if (Util.isNetworkAvailable(getContext())) {
            Log.i("user_id",globalPreferences.getID());
            Log.i("lang",globalPreferences.getLang());
            RetroWeb.getClient().create(ServiceApi.class).getNotification(globalPreferences.getID(),globalPreferences.getLang()).enqueue(new Callback<NotificationModel>() {
                @Override
                public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
//                    swiperefresh.setRefreshing(false);
                    CustomeProgressDialog.onFinish();
                    Log.i("added", "added");
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {
                            if (response.body().getData().size() != 0) {
                                for (NotificationModelData d : response.body().getData()) {
                                    adapter.addNotification(d);
                                    Log.i("added", "added");
                                }
                                rv_notification.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                                rv_notification.setAdapter(adapter);
                                MainActivity.notification_count.setText(String.valueOf(response.body().getData().size()));
                            } else {
                                //TODO NO Data
                                Log.i("NoData", "NoData");

                            }
                        }else {
                            notloading.setVisibility(View.VISIBLE);
                            no_data.setVisibility(View.VISIBLE);
                            Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                            no_data.setText(getString(R.string.no_data));                        }
                    }else{
                        //TODO Null Response
                        Log.i("NullBody", "NullBody");

                    }

                }

                @Override
                public void onFailure(Call<NotificationModel> call, Throwable t) {
                    customeProgressDialog.onFinish();
                    toaster.makeToast("error notiii");
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);

                    Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                    no_data.setText(getString(R.string.no_internent));



                }
            });
        } else {
            Toast.makeText(getContext(), "No Connection", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.notifications);
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new NotificationFragment();
        return frag;
    }

    @Override
    public void OnRefreshNotificationAdapter() {
        adapter = new NotificationAdapter(getContext(),NotificationFragment.this);
        adapter.clearNotifications();
        getNotification();
    }
}
