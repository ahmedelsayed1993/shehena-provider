package com.aait.shehenaprovider.Fragment;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.NewPassword.ChangePasswordModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class ChangePasswordFragment extends BaseFragment {

    @BindView(R.id.ed_new_pass)
    EditText ed_new_pass;
    @BindView(R.id.btn_change)
    Button btn_change;



    private boolean validateNewPassword() {
        if (ed_new_pass.getText().toString().trim().isEmpty()) {
            ed_new_pass.setError(getString(R.string.enter_new_password));
            Util.requestFocus(ed_new_pass, getActivity().getWindow());
            return false;
        }
        else if (ed_new_pass.getText().toString().length()<6)
        {
            ed_new_pass.setError(getString(R.string.enter_password_six));
            Util.requestFocus(ed_new_pass, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validationPassword() {

        if (!validateNewPassword()) {
            return false;
        }
        return true;
    }
    @Override
    protected int getViewId() {
        return R.layout.fragment_change_password;

    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.change_password_title);
    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.btn_change)
    void Change()
    {
        if (validationPassword())
        {
            getNewPassword();
        }
    }

    public void getNewPassword() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNewPassword(globalPreferences.getID(),
                ed_new_pass.getText().toString()).enqueue(new Callback<ChangePasswordModel>() {
            @Override
            public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                Log.e("data",globalPreferences.getID()+"   "+ed_new_pass.getText().toString());
                Util.onPrintLog(response.body());
                CustomeProgressDialog.onFinish();

                if (response.body().getStatus().equals("1")) {
                    toaster.makeToast(response.body().getMessage());
                    MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(ProfileFragment.class.getName()).replace(R.id.content, new ProfileFragment()).commit();
                }
                else
                    toaster.makeToast(response.body().getMessage());

            }

            @Override
            public void onFailure(Call<ChangePasswordModel> call, Throwable t) {

            }

        });
    }


}
