package com.aait.shehenaprovider.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.CongreatulationActivity;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.Process_array;
import com.aait.shehenaprovider.Models.UpdateProcessArrayModel.updateProcessArrModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class FragmentOrderNumberFragment extends BaseFragment {

    @BindView(R.id.btn_cancle)
    Button btn_cancle;
    List<Process_array> Array;
    @BindView(R.id.cv_agree)
    CircleImageView cv_agree;
    @BindView(R.id.cv_away)
    CircleImageView cv_away;
    @BindView(R.id.cv_arrive)
    CircleImageView cv_arrive;
    @BindView(R.id.cv_start)
    CircleImageView cv_start;
    @BindView(R.id.cv_end)
    CircleImageView cv_end;
    @BindView(R.id.cv_complete)
    CircleImageView cv_complete;
    @BindView(R.id.iv_pole1)
    ImageView iv_pole1;
    @BindView(R.id.iv_pole2)
    ImageView iv_pole2;
    @BindView(R.id.iv_pole3)
    ImageView iv_pole3;
    @BindView(R.id.iv_pole4)
    ImageView iv_pole4;
    @BindView(R.id.iv_pole5)
    ImageView iv_pole5;
    @BindView(R.id.tv_date1)
    TextView tv_date1;
    @BindView(R.id.tv_date2)
    TextView tv_date2;
    @BindView(R.id.tv_date3)
    TextView tv_date3;
    @BindView(R.id.tv_date4)
    TextView tv_date4;
    @BindView(R.id.tv_date5)
    TextView tv_date5;
    @BindView(R.id.tv_date6)
    TextView tv_date6;
    @BindView(R.id.tv_agree)
    TextView tv_agree;
    @BindView(R.id.tv_away)
    TextView tv_away;
    @BindView(R.id.tv_arrive)
    TextView tv_arrive;
    @BindView(R.id.tv_start)
    TextView tv_start;
    @BindView(R.id.tv_end)
    TextView tv_end;
    @BindView(R.id.tv_complete)
    TextView tv_complete;
    int index = 0;
    String order_id;
    String[] dateList = new String[6];

    String page;
    String order_num;
    int count = 0;

    @Override
    protected int getViewId() {
        return R.layout.fragment_request_number;
    }

    @Override
    protected void init(View view) {
        Bundle bundle = getArguments();
        page = bundle.getString("page");
        order_id = bundle.getString("order_id");
        order_num = bundle.getString("order_num");

        if (Array == null) {
            Array = (List<Process_array>) bundle.getSerializable("array");
        }
        Util.onPrintLog(dateList);

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.request_num) + " " + order_num;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
        Util.onPrintLog(Array);
        index = 0;
        for (int i = 0; i < Array.size(); i++) {
            Util.onPrintLog(Array.get(i).getStatus());
            if (Array.get(i).getStatus() > 0) {
                index++;
                if (!Array.get(i).getDate().equals("")) {
                    dateList[i] = Array.get(i).getDate();
                }
            }
        }
        Util.onPrintLog(index);
        if (index == 1) {
            number1();
        } else if (index == 2) {
            number2();
        } else if (index == 3) {
            number3();
        } else if (index == 4) {
            number4();
        } else if (index == 5) {
            number5();
        } else if (index == 6) {
            number6();
        }

    }

    @OnClick(R.id.btn_cancle)
    void Cancle() {

        Bundle bundle = new Bundle();
        bundle.putString("order_id", order_id);
        CancleRequestFragment fragment = new CancleRequestFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CancleRequestFragment.class.getName()).replace(R.id.content, fragment).commit();
    }

    @OnClick(R.id.btn_update)
    void Update() {
        if (count <= 6) {
            getUpdateProcessArray();
        }
    }

    void number1() {
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_agree);
        tv_agree.setTextColor(Color.parseColor("#48a2de"));
        tv_date1.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[0])));

    }

    void number2() {
        number1();
        iv_pole1.setBackgroundResource(R.color.colorPrimary);
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_away);
        tv_away.setTextColor(Color.parseColor("#48a2de"));

        tv_date2.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[1])));


    }

    void number3() {
        number2();
        iv_pole2.setBackgroundResource(R.color.colorPrimary);
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_arrive);
        tv_arrive.setTextColor(Color.parseColor("#48a2de"));

        tv_date3.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[2])));


    }

    void number4() {
        number3();
        iv_pole3.setBackgroundResource(R.color.colorPrimary);
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_start);
        tv_start.setTextColor(Color.parseColor("#48a2de"));

        tv_date4.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[3])));


    }

    void number5() {
        number4();
        iv_pole4.setBackgroundResource(R.color.colorPrimary);
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_end);
        tv_end.setTextColor(Color.parseColor("#48a2de"));

        tv_date5.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[4])));


    }

    void number6() {
        number5();
        iv_pole5.setBackgroundResource(R.color.colorPrimary);
        Picasso.with(getActivity()).load(R.mipmap.check_light_blue).into(cv_complete);
        tv_complete.setTextColor(Color.parseColor("#48a2de"));

        tv_date6.setText(Util.getDateCurrentTimeZone(Long.parseLong(dateList[5])));


    }

    public void getUpdateProcessArray() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUpdateProcessArray(order_id).enqueue(new Callback<updateProcessArrModel>() {
            @Override
            public void onResponse(Call<updateProcessArrModel> call, Response<updateProcessArrModel> response) {
                Util.onPrintLog(response.body());
                customeProgressDialog.onFinish();
                Log.e("count_item", count + "");
                if (!response.body().getMessage().equals("Unavailable ID")) {
                    if (response.body().getStaus().equals("1")) {
                        //toaster.makeToast(response.body().getMessage());
                        for (int i = 0; i < response.body().getArray().size(); i++) {
                            Util.onPrintLog(response.body().getArray().get(i).getStatus());
                            count = response.body().getLevel();
                            if (count > 0) {
                                Log.e("count", count + "");
                                dateList[i] = response.body().getArray().get(i).getDate();
                                Util.onPrintLog(response.body().getArray());


                            }
                        }
                        Array = response.body().getArray();

                        Util.onPrintLog(Array);
                        index = count;
                        if (count == 1) {
                            number1();
                        } else if (count == 2) {
                            number2();
                        } else if (count == 3) {
                            number3();
                        } else if (count == 4) {
                            number4();
                        } else if (count == 5) {
                            number5();
                        } else if (count == 6) {
                            number6();
                            count = 7;
                            startActivity(new Intent(getActivity(), CongreatulationActivity.class));
                            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();


                        }


                    } else {
                        toaster.makeToast(response.body().getMessage());
                    }
                }else{
                    toaster.makeToast(getString(R.string.clientCancel));
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();

                }

                CustomeProgressDialog.onFinish();
            }

            @Override
            public void onFailure(Call<updateProcessArrModel> call, Throwable t) {

            }

        });
    }
}
