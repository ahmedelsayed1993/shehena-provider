package com.aait.shehenaprovider.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Adapter.MapCurrentUserWindowAdapter;
import com.aait.shehenaprovider.Adapter.MapNewUserWindowAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.GPS.GPSTracker;
import com.aait.shehenaprovider.GPS.GPSTrakerListner;
import com.aait.shehenaprovider.Models.CurrentOrderModel.CurrentData;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.Order_data;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.User_data;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.newOrdersDetailsModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Aya on 8/1/2017.
 */

public class Map   extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, GPSTrakerListner {
    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.btn_google)
    Button btn_google;
    public static GoogleMap googleMap;
    HashMap<Marker, User_data> hmap1;
    HashMap<Marker, CurrentData> hmap2;

    MapNewUserWindowAdapter mapNewUserWindowAdapter;
    MapCurrentUserWindowAdapter mapCurrentUserWindowAdapter;
    GPSTracker gps;
    Marker marker;
    String id;
    List<User_data>user_datas ;
    List<Order_data>order_datas ;
    Bundle bundle;
    String page;
    CurrentData currentOrder_data;
    String name,img,lat,lng;
    String title="";

    @Override
    protected int getViewId() {
        return R.layout.fragment_map;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void init(View view) {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        id = getArguments().getString("id");
        page=getArguments().getString("page");
        title=getArguments().getString("service");
        //toaster.makeToast(page);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Initialize();

    }


    @Override
    protected String SetTitle() {
        return title;
    }


    @Override
    public void onClick(View view) {

    }

    public void Initialize() {
        hmap1 = new HashMap<>();
        hmap2 = new HashMap<>();

    }

    private void AddMarkersToAjeer(User_data model) {

        marker = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(-27.47093, 153.0235)));

            marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker));

        hmap1.put(marker, model);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getLocaionWithPermission();
        if (page.equals("new")) {
            getNewOrderDetails();
        }
        else if (page.equals("current"))
        {
            getCurrentOrderDetails();
        }

    }
    @OnClick(R.id.btn_google)
    void Google() {
        if (page.equals("new")) {
            Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr="+order_datas.get(0).getLat()+","+order_datas.get(0).getLng());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
        else  if (page.equals("current")) {
            Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr="+currentOrder_data.getLat()+","+currentOrder_data.getLng());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }

        }
    public void onMarker(){
        if (page.equals("new")) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(
                    new LatLng(Double.parseDouble(order_datas.get(0).getLat()), Double.parseDouble(order_datas.get(0).getLng())));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(order_datas.get(0).getLat()), Double.parseDouble(order_datas.get(0).getLng()))));
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker));
            hmap1.put(marker, user_datas.get(0));
        }
        else  if (page.equals("current")) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(
                    new LatLng(Double.valueOf(currentOrder_data.getLat()), Double.valueOf(currentOrder_data.getLng())));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(
                            new LatLng(Double.valueOf(currentOrder_data.getLat()), Double.valueOf(currentOrder_data.getLng()))));
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker));
            hmap2.put(marker, currentOrder_data);
        }
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }

    private void putuserMarkerInMap(Double lat, Double log) {
        putMapMarker(lat, log);
    }

    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.map_marker));
        googleMap.addMarker(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (hmap1.containsKey(marker)||hmap2.containsKey(marker)) {
            if (page.equals("new")) {
                User_data model = hmap1.get(marker);
                mapNewUserWindowAdapter = new MapNewUserWindowAdapter(getActivity(), model);
                googleMap.setInfoWindowAdapter(mapNewUserWindowAdapter);

            }
            else if (page.equals("current")) {
                CurrentData model = hmap2.get(marker);
                mapCurrentUserWindowAdapter = new MapCurrentUserWindowAdapter(getActivity(), model);
                googleMap.setInfoWindowAdapter(mapCurrentUserWindowAdapter);

            }

        } else {
            googleMap.setInfoWindowAdapter(null);
        }
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
            OrderDetailsFragment fragment = new OrderDetailsFragment();
            fragment.setArguments(bundle);
            MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(OrderDetailsFragment.class.getName()).replace(R.id.content, fragment).commit();
    }




    @Override
    public void onTrakerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTraker() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getLocaionWithPermission() {
        gps = new GPSTracker(getActivity(), this);
        if (canMakeSmores()) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                List<String> permissionsNeeded = new ArrayList<String>();
                final List<String> permissionsList = new ArrayList<String>();
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
                    permissionsNeeded.add("GPS");
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
                    permissionsNeeded.add("Location");

                if (permissionsList.size() > 0) {
                    if (permissionsNeeded.size() > 0) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                100);
                    }
                }
                Log.e("GPS", "1");
            } else {
                Log.e("GPS", "2");
                getCurrentLocation();
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    // run time permission
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            showSettingsAlert();
            Log.e("Settings", "Settings");
        } else {
        }

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 300);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    public void getNewOrderDetails() {
          CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrderDetails(id,globalPreferences.getLang()).enqueue(new Callback<newOrdersDetailsModel>() {
            @Override
            public void onResponse(Call<newOrdersDetailsModel> call, Response<newOrdersDetailsModel> response) {

                user_datas=response.body().getData().getUser_data();
                order_datas=response.body().getData().getOrder_data();
                Util.onPrintLog(response.body());
                onMarker();
                mapNewUserWindowAdapter = new MapNewUserWindowAdapter(getActivity(), user_datas.get(0));
                customeProgressDialog.onFinish();
                bundle = new Bundle();
                bundle.putString("page","new");
                bundle.putSerializable("order_data",response.body().getData().getOrder_data().get(0));
                bundle.putSerializable("user_data", response.body().getData().getUser_data().get(0));
                bundle.putString("status",response.body().getData().getOrder_processing()+"");
                bundle.putString("id",id);
                }
            @Override
            public void onFailure(Call<newOrdersDetailsModel> call, Throwable t) {
                 customeProgressDialog.onFinish();

            }

        });
    }
    public void getCurrentOrderDetails() {
        currentOrder_data= (CurrentData) getArguments().getSerializable("model");
        onMarker();
        mapCurrentUserWindowAdapter = new MapCurrentUserWindowAdapter(getActivity(), currentOrder_data);
        customeProgressDialog.onFinish();
        bundle = new Bundle();
        bundle.putString("page","current");
        bundle.putSerializable("data", (Serializable) currentOrder_data);
        bundle.putString("id",id);

    }

}