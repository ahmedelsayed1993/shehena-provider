package com.aait.shehenaprovider.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MapAddressActivity;
import com.aait.shehenaprovider.Adapter.AssistantAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Listners.IAssistantFragment;
import com.aait.shehenaprovider.Models.AddAss;
import com.aait.shehenaprovider.Models.Categories.CategoriesHeadResponse;
import com.aait.shehenaprovider.Models.ProfileModel.Assistants;
import com.aait.shehenaprovider.Models.ProfileModel.profileModel;
import com.aait.shehenaprovider.Models.RegisterModel.AssistantNameModel;
import com.aait.shehenaprovider.Models.SubCategories.SubCategoriesDataResponse;
import com.aait.shehenaprovider.Models.SubCategories.SubCategoriesHeadResponse;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Constant;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class ProfileFragment extends BaseFragment implements IAssistantFragment {
    @BindView(R.id.ll_help)
    LinearLayout layout;
    @BindView(R.id.iv_cam_file)
    ImageView iv_cam_file;
    @BindView(R.id.iv_cam_license)
    ImageView iv_cam_license;
    @BindView(R.id.cv_photo)
    CircleImageView cv_photo;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.iv_edit_name)
    ImageView iv_edit_name;
    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.iv_edit_email)
    ImageView iv_edit_email;
    @BindView(R.id.ed_city)
    EditText ed_city;
    @BindView(R.id.iv_edit_city)
    ImageView iv_edit_city;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.iv_edit_phone)
    ImageView iv_edit_phone;
    @BindView(R.id.ed_paeeword)
    EditText ed_paeeword;
    @BindView(R.id.iv_edit_pass)
    ImageView iv_edit_pass;
    @BindView(R.id.ed_id_number)
    EditText ed_id_number;
    @BindView(R.id.iv_edit_id)
    ImageView iv_edit_id;
    @BindView(R.id.ed_plate_number)
    EditText ed_plate_number;
    @BindView(R.id.iv_edit_plate)
    ImageView iv_edit_plate;
    @BindView(R.id.sp_sub_service)
    Spinner sp_sub_service;
    @BindView(R.id.sp_driving_license)
    Spinner sp_driving_license;
    @BindView(R.id.iv_edit_type)
    ImageView iv_edit_type;
    @BindView(R.id.ed_service_price)
    EditText ed_service_price;
    @BindView(R.id.ed_help)
    EditText ed_help;
    @BindView(R.id.ed_job)
    EditText ed_job;
    @BindView(R.id.iv_edit_price)
    ImageView iv_edit_price;
    @BindView(R.id.btn_save)
    Button btn_save;
    @BindView(R.id.tv_add)
    TextView tv_add;
    @BindView(R.id.ed_notes)
    EditText ed_notes;
    @BindView(R.id.ed_size)
    EditText ed_size;
    @BindView(R.id.iv_edit_size)
    ImageView iv_edit_size;
    @BindView(R.id.iv_edit_notes)
    ImageView iv_edit_notes;
    String id_category = "";
    String id_subcategory = "";
    ArrayList<String> categories = new ArrayList<>();
    String[] spinnerItemsForCategories;
    ArrayList<String> sub_categories = new ArrayList<>();
    String[] spinnerItemsForSubCategories;
    String Category = "";
    String Sub_Category = "";
    int sub_position;
    int position;
    int numberOfTextViews = 0;
    int count = 20;
    EditText[] editTexts = new EditText[count];
    Assistants assistantNameModel;
    EditText editText;
    int assistantNum;
    profileModel profileData;
    List<Assistants> strings;
    List<String> text;
    RelativeLayout.LayoutParams lparams;
    public String filePath, profileImageFilePath;
    String ImageBaseLicense = "";
    String ImageBasefile = "";
    String Logo = "";
    int mA = 0;
    String latiud, langitud;
    String city;
    AssistantNameModel assistantName;
    @BindView(R.id.rv_assistants)
    RecyclerView rv_assistants;
    AssistantAdapter adapter;

    @Override
    protected int getViewId() {
        return R.layout.fragment_profile;
    }

    @OnClick(R.id.iv_edit_city)
    void City() {
        Intent intent = new Intent(getActivity(), MapAddressActivity.class);
        startActivityForResult(intent, Constant.RequestCode.GETLOCATION);
    }


    private boolean validationUpdate() {
        if (!validateName()) {
            return false;
        }
        if (!validateEmail()) {
            return false;
        }

        if (!validateCity()) {
            return false;
        }
       /* if (!LatLng()) {
            return false;
        }*/
        if (!validatePhone()) {
            return false;
        }
        if (!validateIdentity()) {
            return false;
        }
        if (!validatePlate()) {
            return false;
        }
        if (!validatePrice()) {
            return false;
        }

        return true;
    }

    private boolean validateIdentity() {
        if (ed_id_number.getText().toString().trim().isEmpty()) {
            ed_id_number.setError(getString(R.string.enter_id));
            Util.requestFocus(ed_id_number, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validatePlate() {
        if (ed_plate_number.getText().toString().trim().isEmpty()) {
            ed_plate_number.setError(getString(R.string.enter_plate));
            Util.requestFocus(ed_plate_number, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validatePrice() {
        if (ed_service_price.getText().toString().trim().isEmpty()) {
            ed_service_price.setError(getString(R.string.enter_price));
            Util.requestFocus(ed_service_price, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean LatLng() {
        if (latiud == null || langitud == null) {
            //Log.e(TAG, "LatLng = null");
            return false;
        } else {
            return true;
        }
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        } else if (ed_phone.getText().toString().length() < 8) {
            ed_phone.setError(getString(R.string.enter_phone_eight));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }

        return true;
    }


    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            ed_name.setError(getString(R.string.enter_name));
            Util.requestFocus(ed_name, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validateCity() {
        if (ed_city.getText().toString().trim().isEmpty()) {
            ed_city.setError(getString(R.string.enter_city));
            Util.requestFocus(ed_city, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validateEmail() {
        if (ed_email.getText().toString().trim().isEmpty()) {
            ed_email.setError(getString(R.string.enter_email));
            Util.requestFocus(ed_email, getActivity().getWindow());
            return false;
        } else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(ed_email.getText().toString()).matches()) {
                ed_email.setError(getString(R.string.email_formate));
                Util.requestFocus(ed_email, getActivity().getWindow());
                return false;
            }
        }
        return true;
    }

    @OnClick(R.id.iv_cam_license)
    void License() {
        mA = 1;
        showPictureDialog();
    }

    @OnClick(R.id.iv_cam_file)
    void File() {
        mA = 2;
        showPictureDialog();
    }

    @OnClick(R.id.cv_photo)
    void Photo() {
        mA = 3;
        showPictureDialog();
    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);
    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            if (mA == 1) {
                handleChoosePhotoFromGallery(data, iv_cam_license);
            } else if (mA == 2) {
                handleChoosePhotoFromGallery(data, iv_cam_file);
            } else if (mA == 3) {
                handleChoosePhotoFromGallery(data, cv_photo);
            }

        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            if (mA == 1) {
                handleTakePhotoFromCamera(data, iv_cam_license);
            } else if (mA == 2) {
                handleTakePhotoFromCamera(data, iv_cam_file);
            } else if (mA == 3) {
                handleTakePhotoFromCamera(data, cv_photo);
            }
        } else if (requestCode == Constant.RequestCode.GETLOCATION) {
            // Log.d("jj", mAdresse + mLat + mLang);

            if (data != null) {
                city = data.getStringExtra(Constant.LOCATION);
                //mLang = data.getStringExtra(Constant.LNG);
                //mLat = data.getStringExtra(Constant.LAT);
                ed_city.setText(city);
            }

        }
    }

    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void setPic(ImageView mImageView, String currentPath) {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        if (mA == 1) {
            ImageBaseLicense = Util.getEncoded64ImageStringFromBitmap(bitmap);
        } else if (mA == 2) {
            ImageBasefile = Util.getEncoded64ImageStringFromBitmap(bitmap);
        } else if (mA == 3) {
            Logo = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
    }

    private void handleTakePhotoFromCamera(Intent data, ImageView mImageView) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        Uri tempUri = getImageUri(getActivity(), imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        setPic(mImageView, finalFile.getAbsolutePath());
        if (mA == 1) {
            ImageBaseLicense = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        } else if (mA == 2) {
            ImageBasefile = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        } else if (mA == 3) {
            Logo = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void init(View view) {
        strings = new ArrayList<>();

        getProfile();
        adapter = new AssistantAdapter(getContext(),ProfileFragment.this);
       // getCategories();
        //getSubCategory();
    }
    @Override
    public void OnRefreshAssAdapter() {
        adapter = new AssistantAdapter(getContext(),ProfileFragment.this);
        getProfile();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.nav_profile);
    }

    @Override
    public void onClick(View view) {


    }

    @OnClick(R.id.iv_edit_name)
    void name() {
        ed_name.setEnabled(true);

    }
    @OnClick(R.id.tv_add)
    void Add() {
        if (ed_help.getText().toString().isEmpty()&&ed_job.getText().toString().isEmpty())
        {}
        else {
            getAddAss();


        }

    }
    @OnClick(R.id.iv_edit_email)
    void email() {
        ed_email.setEnabled(true);

    }



    @OnClick(R.id.iv_edit_phone)
    void phone() {
        ed_phone.setEnabled(true);

    }

    @OnClick(R.id.iv_edit_pass)
    void password() {
        ed_paeeword.setEnabled(true);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(ChangePasswordFragment.class.getName()).replace(R.id.content, new ChangePasswordFragment()).commit();


    }

    @OnClick(R.id.btn_save)
    void save() {
        if (validationUpdate()) {

            strings.clear();
            for (int i = 0; i < editTexts.length; i++) {
                Log.e("number", assistantNum + "");
                Assistants model = new Assistants();
                if (editTexts[i] != null) {
                    if (!editTexts[i].getText().toString().equals("")) {
                        model.setName(editTexts[i].getText().toString());
                        strings.add(model);
                    }

                }
            }
            Util.onPrintLog(strings);
            getUpdate();

        }
    }


    @OnClick(R.id.iv_edit_id)
    void id() {
        ed_id_number.setEnabled(true);

    }

    @OnClick(R.id.iv_edit_plate)
    void plate() {
        ed_plate_number.setEnabled(true);

    }


    @OnClick(R.id.iv_edit_type)
    void type() {

    }

    @OnClick(R.id.iv_edit_price)
    void price() {
        ed_service_price.setEnabled(true);

    }
    @OnClick(R.id.iv_edit_size)
    void size() {
        ed_size.setEnabled(true);

    }
    @OnClick(R.id.iv_edit_notes)
    void notes() {
        ed_notes.setEnabled(true);

    }
    //getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(BookRoomFragment.class.getName()).replace(R.id.container, new CelanderFragment()).commit();
    public void SetSpinnerCategory() {
        Util.onPrintLog("pos"+position);
        ArrayAdapter<CharSequence> category_adapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForCategories
        );
        sp_driving_license.setAdapter(category_adapter);
        sp_driving_license.setSelection(position);
    }

    public void getCategories() {
        //CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(globalPreferences.getLang()).enqueue(new Callback<CategoriesHeadResponse>() {
            @Override
            public void onResponse(Call<CategoriesHeadResponse> call, final Response<CategoriesHeadResponse> response) {
                //customeProgressDialog.onFinish();
                Util.onPrintLog(response.body());
                categories = new ArrayList<String>();
                if (response.body().getData() != null) {

                    for (int i = 0; i < response.body().getData().size(); i++) {
                        categories.add(response.body().getData().get(i).getName());
                        //   Log.e("category",Category +"   "+arrayList.get(i).getAr_Name() +"   " +i);
                        if (response.body().getData().get(i).getId() == position) {
                            position = i;
                            //   Log.e("id",i+"");
                        }
                    }
                    Log.e("index", "" + categories.size());
                    spinnerItemsForCategories = new String[categories.size()];
                    spinnerItemsForCategories = categories.toArray(spinnerItemsForCategories);
                    SetSpinnerCategory();
                } else {
                   // customeProgressDialog.onFinish();
                }
                sp_driving_license.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_category = "" + response.body().getData().get(position).getId();
                        sub_categories.clear();
                        id_subcategory = "";
                        getSubCategory();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(Call<CategoriesHeadResponse> call, Throwable t) {
                // customeProgressDialog.onFinish();
            }

        });
    }

    public void SetSpinnerSubCategory() {
        ArrayAdapter<CharSequence> sub_category_adapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForSubCategories
        );
        sp_sub_service.setAdapter(sub_category_adapter);
        sp_sub_service.setSelection(sub_position);
    }

    public void getSubCategory() {
        //CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSubCategory(globalPreferences.getLang(), id_category).enqueue(new Callback<SubCategoriesHeadResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesHeadResponse> call, Response<SubCategoriesHeadResponse> response) {
              //  customeProgressDialog.onFinish();
                Util.onPrintLog("2");
                final ArrayList<SubCategoriesDataResponse> arrayList = (ArrayList<SubCategoriesDataResponse>) response.body().getData();
                Util.onPrintLog(arrayList);
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        sub_categories.add(arrayList.get(i).getName());
                        if (arrayList.get(i).getName().equals(Sub_Category)) {
                            sub_position = i;
                        }
                    }
                    spinnerItemsForSubCategories = new String[sub_categories.size()];
                    spinnerItemsForSubCategories = sub_categories.toArray(spinnerItemsForSubCategories);
                    SetSpinnerSubCategory();
                } else {
                    //customeProgressDialog.onFinish();
                }
                sp_sub_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_subcategory = arrayList.get(position).getId()+"";
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(Call<SubCategoriesHeadResponse> call, Throwable t) {
                //customeProgressDialog.onFinish();
            }

        });
    }

    void dynamicEditText() {
        lparams = new RelativeLayout.LayoutParams
                (RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lparams.addRule(RelativeLayout.BELOW, editTexts[numberOfTextViews].getId());
        lparams.setMargins(0, 10, 0, 0);
        editText = new EditText(getActivity());
        assistantNameModel = new Assistants();
        editText.setHint(getString(R.string.another_help_name));
        editText.setId(++numberOfTextViews);
        editText.setTextSize(16);
        editText.setTextColor(Color.BLACK);
        editText.setBackgroundResource(R.drawable.round_rectangle);
        editText.setGravity(Gravity.RIGHT);
        editTexts[numberOfTextViews] = editText;
        layout.addView(editTexts[numberOfTextViews], lparams);

    }

    void getAssistants() {
        for (int i = 0; i < assistantNum; i++) {
            Log.e("number", assistantNum + "");
            Assistants model = new Assistants();
            model.setName(profileData.getData().getAssistants().get(i).getName());
            model.setJob("jjj");
            strings.add(model);
            dynamicEditText();
            editText.setText(profileData.getData().getAssistants().get(i).getName());
            assistantNameModel.setName(profileData.getData().getAssistants().get(i).getName());
            editText.setEnabled(true);


            //strings.add(assistantNameModel);
            // Util.onPrintLog(strings);
        }
    }


    public void getProfile() {
       // CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        Util.onPrintLog(globalPreferences.getID());
        RetroWeb.getClient().create(ServiceApi.class).
                getProfile(globalPreferences.getLang(), globalPreferences.getID()).enqueue(new Callback<profileModel>() {
            @Override
            public void onResponse(Call<profileModel> call, Response<profileModel> response) {
                Log.e("data", globalPreferences.getID() + "   " + globalPreferences.getLang());
                Util.onPrintLog(response.body());
                if (response.body().getStatus().equals("1")) {
                   // CustomeProgressDialog.onFinish();
                    ed_name.setText(response.body().getData().getName());
                    ed_email.setText(response.body().getData().getEmail());
                    ed_city.setText(response.body().getData().getAddress());
                    ed_phone.setText(response.body().getData().getPhone());
                    ed_id_number.setText(response.body().getData().getIdentity_num());
                    ed_plate_number.setText(response.body().getData().getCar_num());
                    ed_service_price.setText(response.body().getData().getPrice());
                    ed_size.setText(response.body().getData().getCar_size());
                    if  (response.body().getData().getNotes()!=null) {
                        ed_notes.setText(response.body().getData().getNotes());
                    }
                    else    {
                        ed_notes.setText("");
                    }
                    Picasso.with(getActivity()).load(response.body().getData().getIdentity_image()).into(iv_cam_license);
                    Picasso.with(getActivity()).load(response.body().getData().getInsurance_file()).into(iv_cam_file);
                    Picasso.with(getActivity()).load(response.body().getData().getAvatar()).into(cv_photo);
                    Sub_Category = response.body().getData().getService_name();
                    sub_position = response.body().getData().getsubcategory_id();
                    position = response.body().getData().getCategory_id();
                    assistantNum = response.body().getData().getAssistants().size();
                    Log.e("ass",assistantNum+"");
                    if (assistantNum==0)
                        ed_help.setVisibility(View.VISIBLE);
                    adapter.clearArray();

                    Log.e("positoion", "" + position);
                    //getAssistants();
                    getCategories();
                    for (Assistants m : response.body().getData().getAssistants()) {
                        adapter.addItem(m);
                    }
                    rv_assistants.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_assistants.setAdapter(adapter);

                } else {
                   // CustomeProgressDialog.onFinish();

                }
            }

            @Override
            public void onFailure(Call<profileModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();
            }

        });
    }

    public void getUpdate() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUpdate(globalPreferences.getID(), ed_name.getText().toString(),
                ed_email.getText().toString(), city, latiud, langitud, ed_phone.getText().toString(),
                ed_plate_number.getText().toString(), ed_id_number.getText().toString(), ed_service_price.getText().toString()
                , id_subcategory, ImageBaseLicense, ImageBasefile, Logo,ed_size.getText().toString(),ed_notes.getText().toString()).enqueue(new Callback<profileModel>() {
            @Override
            public void onResponse(Call<profileModel> call, Response<profileModel> response) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(response.body());
                if (response.body() != null) {
                    if (response.body().getValue().equals("1")) {
                        toaster.makeToast(getString(R.string.save_done));
                        CustomeProgressDialog.onFinish();
                        globalPreferences.storeID(response.body().getData().getProvider_id()+ "");
                        globalPreferences.storeNAME(response.body().getData().getName());
                        globalPreferences.storeEMAIL(response.body().getData().getEmail());
                        globalPreferences.storePHONE(response.body().getData().getPhone());
                        globalPreferences.storeLAT(response.body().getData().getLat());
                        globalPreferences.storeLNG(response.body().getData().getLng());
                        globalPreferences.storeADDRESS(response.body().getData().getAddress());
                        globalPreferences.storeCAR_NUM(response.body().getData().getCar_num());
                        globalPreferences.storeIDENTITY_NUM(response.body().getData().getIdentity_num());
                        globalPreferences.storePrice(response.body().getData().getPrice());
                        globalPreferences.storeSUB_CATEGORY_ID(response.body().getData().getsubcategory_id() + "");
                        globalPreferences.storeAVATAR(response.body().getData().getAvatar());
                        globalPreferences.storeIDENTITY_IMAGE(response.body().getData().getIdentity_image());
                        globalPreferences.storeINSURANCE_FILE(response.body().getData().getInsurance_file());

                    } else {
                        toaster.makeToast(response.body().getMsg());
                        CustomeProgressDialog.onFinish();

                    }

                }
            }

            @Override
            public void onFailure(Call<profileModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(t.getMessage());
            }

        });
    }
    public void getAddAss() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAddAss(globalPreferences.getID(),ed_help.getText().toString(),ed_job.getText().toString()).enqueue(new Callback<AddAss>() {
            @Override
            public void onResponse(Call<AddAss> call, Response<AddAss> response) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(response.body());
                if (response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        toaster.makeToast(getString(R.string.save_done));
                        CustomeProgressDialog.onFinish();
                        getProfile();
                        ed_help.setText("");
                        ed_job.setText("");


                    } else {
                        // toaster.makeToast(response.body().getData().getMessage());
                        CustomeProgressDialog.onFinish();

                    }

                }
            }

            @Override
            public void onFailure(Call<AddAss> call, Throwable t) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(t.getMessage());
            }

        });
    }

}
