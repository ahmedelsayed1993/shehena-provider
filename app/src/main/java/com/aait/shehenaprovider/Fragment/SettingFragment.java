package com.aait.shehenaprovider.Fragment;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import com.aait.shehenaprovider.Activities.SplashActivity;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.SettingNotificationChangeResponse;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aya on 6/9/2017.
 */

public class SettingFragment extends BaseFragment {
@BindView(R.id.sp_language)
    Spinner sp_language;
    @BindView(R.id.toggle)
    Switch toggle;
    String state="";
    @Override
    protected int getViewId() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void init(View view) {
        notification();
        language();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.settings);
    }

    @Override
    public void onClick(View view) {

    }
    public void onResetActivity(){
        Intent intent = new Intent(getActivity(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    void language()
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.language, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_language.setAdapter(adapter);
        sp_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                String value = (String) parentView.getItemAtPosition(position);
                Log.i("lang", value);
                if (value.contains("English")) {
                    if (!globalPreferences.getLang().equals("en")) {
                        globalPreferences.setLang("en");
                    }
                } else {
                    if (!globalPreferences.getLang().equals("ar")) {
                        globalPreferences.setLang("ar");
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }
    void notification() {
        if (globalPreferences.getNotification().equals("1")) {
            toggle.setChecked(true);

        } else {
            toggle.setChecked(false);

        }
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (toggle.isChecked()) {
                    globalPreferences.storeNotification("1");

                } else {
                    globalPreferences.storeNotification("0");

                }


            }
        });
    }


    @OnClick(R.id.btn_saveSettings)
    void saveChanges(){
        if(Util.isNetworkAvailable(getContext())) {
//            ll_no_Wifi.setVisibility(View.GONE);
//            user_id = globalPreferences.getUserId();
            RetroWeb.getClient().create(ServiceApi.class).settings(globalPreferences.getID(),globalPreferences.getLang(),globalPreferences.getNotification()).enqueue(new Callback<SettingNotificationChangeResponse>() {
                @Override
                public void onResponse(Call<SettingNotificationChangeResponse> call, Response<SettingNotificationChangeResponse> response) {
                    if (response.body().getStatus().equals("1")){
                        toaster.makeToast(getString(R.string.SettingsSuccessfulyUpdated));
                        onResetActivity();
                    }else{
                        toaster.makeToast(getString(R.string.SettingsNotSuccessfulyUpdated));
                    }
                }

                @Override
                public void onFailure(Call<SettingNotificationChangeResponse> call, Throwable t) {
                    toaster.makeToast(getString(R.string.SomeThingWentWrong));
                }
            });
        }else{
            toaster.makeToast(getString(R.string.NoInternetConnection));
//            ll_no_Wifi.setVisibility(View.VISIBLE);
//            ll_contactUs.setVisibility(View.GONE);
        }
    }


}
