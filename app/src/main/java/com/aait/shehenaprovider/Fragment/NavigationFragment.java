package com.aait.shehenaprovider.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Adapter.NavigationDrawerAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Components.LitterAsImage;
import com.aait.shehenaprovider.Listners.NavigationActionListner;
import com.aait.shehenaprovider.Models.CurrentOrderModel.currentModel;
import com.aait.shehenaprovider.Models.MenuModel;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 12/1/2017.
 */

public class NavigationFragment extends BaseFragment {
    public static String TAG = NavigationFragment.class.getSimpleName();
    @BindView(R.id.menu_recycle)
    RecyclerView menu_recycle;
    @BindView(R.id.civ_user_img)
    CircleImageView civ_user_img;
    @BindView(R.id.tv_username)
    TextView tv_username;

    @BindView(R.id.head)
    RelativeLayout head;
    ArrayList<MenuModel> data;
    NavigationDrawerAdapter drawerAdapter;
   static String order_number;
    static String notification_number;


    @Override
    protected int getViewId() {
        return R.layout.fragment_navigation_menu;
    }

    @Override
    protected void init(View view) {


        if (globalPreferences.getLoginStatus()) {

        } else {
            head.setVisibility(View.GONE);
        }


    }

    @Override
    public void onResume() {
        getOrders();
        UpdateDrawer();
        setMenuData();

        super.onResume();
        Log.e(TAG, "Resume");

    }

    @Override
    protected String SetTitle() {
        return null;
    }

    @Override
    public void onClick(View view) {
    }



    public void setMenuData() {
        data = new ArrayList<>();
        data.add(new MenuModel(R.mipmap.side_nav_profile, getString(R.string.profile),""));
        data.add(new MenuModel(R.mipmap.side_nav_notifications, getString(R.string.notification),""));
        data.add(new MenuModel(R.mipmap.side_nav_current_orders, getString(R.string.orders),order_number));
        data.add(new MenuModel(R.mipmap.side_nav_current_orders, getString(R.string.new_orders),""));
        data.add(new MenuModel(R.mipmap.side_nav_current_orders, getString(R.string.finish_orders),""));
        data.add(new MenuModel(R.mipmap.service_provider_side_nav_money, getString(R.string.accounts),""));
        data.add(new MenuModel(R.mipmap.side_nav_settings, getString(R.string.setting),""));
        data.add(new MenuModel(R.mipmap.side_nav_about, getString(R.string.about),""));
        data.add(new MenuModel(R.mipmap.side_nav_share, getString(R.string.share),""));
        data.add(new MenuModel(R.mipmap.side_nav_signout, getString(R.string.logout),""));


        menu_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter((AppCompatActivity) getActivity(), data, navigationActionListner);
        menu_recycle.setAdapter(drawerAdapter);
    }

    NavigationActionListner navigationActionListner = new NavigationActionListner() {
        @Override
        public void onProfile() {
            replaceFragment(new ProfileFragment());
        }

        @Override
        public void onNotification() {
            replaceFragment(new NotificationFragment());

        }

        @Override
        public void onAccounts() {
            replaceFragment(new AccountsFragment());

        }

        @Override
        public void onRequestes() {
            replaceFragment(new CurrentOrdersFragment());

        }

        @Override
        public void onNewRequestes() {
            replaceFragment(new NewOrdersFragment());

        }

        @Override
        public void onFinishRequestes() {
            replaceFragment(new FinishedOrdersFragment());


        }

        @Override
        public void onSettings() {
            replaceFragment(new SettingFragment());

        }

        @Override
        public void onAbout() {
            replaceFragment(new AboutFragment());


        }

        @Override
        public void onShare() {
            Util.ShareApp(getActivity());
        }

        @Override
        public void onLogout() {
            logout(getActivity());
        }

    };

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        boolean exist = false;
        for (int i = 0; i < MainActivity.activity.getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (MainActivity.activity.getSupportFragmentManager().getBackStackEntryAt(i).getName() != null)
                if (MainActivity.activity.getSupportFragmentManager().getBackStackEntryAt(i).getName().equals(backStateName)) {
                    exist = true;
                    break;
                }
        }
        if (!exist) {
            MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(backStateName).replace(R.id.content, fragment).commit();
        } else
            MainActivity.activity.getSupportFragmentManager().popBackStackImmediate(backStateName, 0);
    }

    public void logout(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //Setting message manually and performing action on button click
        builder.setMessage(getString(R.string.logout_message))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes_answer), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        globalPreferences.LogOut();
                        globalPreferences.storeLoginStatus(false);
                        MainActivity.activity.finish();
                        //  context.startActivity(new Intent(context, LoginActivity.class));
                    }
                })
                .setNegativeButton(getString(R.string.no_answer), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }



    public void getOrders() {
        RetroWeb.getClient().create(ServiceApi.class).getCurrentOrders(globalPreferences.getID(),globalPreferences.getLang()).enqueue(new Callback<currentModel>() {
            @Override
            public void onResponse(Call<currentModel> call, Response<currentModel> response) {

                        order_number = response.body().getData().size() + "";
                Log.e("kk",order_number);
                getNotification();
            }
            @Override
            public void onFailure(Call<currentModel> call, Throwable t) {

            }

        });
    }
    public void getNotification() {
        RetroWeb.getClient().create(ServiceApi.class).getNotification(globalPreferences.getID(),globalPreferences.getLang()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                Log.i("Languagee",globalPreferences.getLang());
                if (response.body() != null) {
                    if (response.body() != null) {
                        if (response.body().getData()!=null){
                            notification_number = response.body().getData().size() + "";
                            Log.e("kk",notification_number);

                        }
                        setMenuData();
                    } else {
                        Log.i("NullData", "No Data in getNotifications");
                    }
                }else{
                    Log.i("NullData","Null body in NavigationFragment");
                }
            }
            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                toaster.makeToast("error failure navi noti");
            }

        });
    }
    public void UpdateDrawer() {
        String name=globalPreferences.getNAME();
        String image=globalPreferences.getAVATAR();
            tv_username.setText(name);
            if (!name.equals("")) {
                Picasso.with(getActivity()).load(image).into(civ_user_img);
            } else {
                LitterAsImage litterAsImage = new LitterAsImage(getActivity());
                if (!name.equals("")) {
                    Bitmap bitmap = litterAsImage.litterToImage(R.drawable.oval_colorprimary, name);
                    civ_user_img.setImageBitmap(bitmap);
                }
            }

    }

    public void drawerOpenClose() {
        if (globalPreferences.getLang().equals("ar") || globalPreferences.getLang().equals("en")) {
            MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            MainActivity.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }
}