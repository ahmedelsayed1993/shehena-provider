package com.aait.shehenaprovider.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Adapter.AboutAdapter;
import com.aait.shehenaprovider.Base.BaseFragment;
import com.aait.shehenaprovider.Models.AboutModel.aboutModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class AboutFragment extends BaseFragment {

    @BindView(R.id.tv_cong)
    TextView tv_cong;
    @BindView(R.id.tv_desc)
    TextView tv_desc;
    @BindView(R.id.tv_progress)
    TextView tv_progress;
    @BindView(R.id.cv_photo)
    ImageView cv_photo;
    String facebook,twitter,youtube,instgram,google;
    @BindView(R.id.rv_about)
    RecyclerView rv_about;
    AboutAdapter adapter;
    int allSocial;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;


    @Override
    protected int getViewId() {
        return R.layout.fragment_about;

    }

    @Override
    protected void init(View view) {
        getAboutApp();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.about_app);
    }

    @Override
    public void onClick(View view) {

    }
    private void openURL(String url) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            MainActivity.activity.startActivity(browserIntent);
        } catch (Exception e) {
            Log.e("Exption", e.toString());
        }

    }
    public void getAboutApp() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAboutApp(globalPreferences.getLang()).enqueue(new Callback<aboutModel>() {
            @Override
            public void onResponse(Call<aboutModel> call, Response<aboutModel> response) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(response.body());
               // Picasso.with(getActivity()).load(response.body().getData().getLogo()).into(cv_photo);
                tv_desc.setText(response.body().getData().getAbout());
                allSocial=response.body().getData().getAllSocial().size();
                adapter = new AboutAdapter(getContext(), response.body().getData().getAllSocial());
                rv_about.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                rv_about.setAdapter(adapter);
                if (response.body().getData().getAllSocial().size()==0)
                {
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);

                    Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                    no_data.setText(getString(R.string.no_data));
                }
            }

            @Override
            public void onFailure(Call<aboutModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);

                Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));
            }

        });
    }


}
