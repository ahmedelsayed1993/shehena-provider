package com.aait.shehenaprovider.Base;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.aait.shehenaprovider.Widget.Toaster;
import java.util.Locale;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/*
created by mahmoud 10/4/2017
 */

public abstract class ParentActivity extends AppCompatActivity {

    protected GlobalPreferences globalPreferences;
    private int menuId;
    protected Toaster toaster;
    protected Bundle savedInstanceState;
    protected CustomeProgressDialog customeProgressDialog;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isFullScreen()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        if (hideInputeType()) {
            hideInputtype();
        }
        // set layout resourses
        setContentView(getLayoutResource());

        globalPreferences = new GlobalPreferences(this);
        toaster = new Toaster(this);
        customeProgressDialog = new CustomeProgressDialog(this, "");
        this.savedInstanceState = savedInstanceState;
        ButterKnife.bind(this);

        //language
        setLocale(globalPreferences.getLang());


        // initialize item
        init();

    }

    protected abstract int getLayoutResource();

    protected abstract boolean isFullScreen();

    protected abstract void init();

    protected abstract boolean isEnableBack();


    protected abstract boolean hideInputeType();


    public void createOptionsMenu(int menuId) {
        this.menuId = menuId;
        invalidateOptionsMenu();
    }

    /**
     * function is used to create a menu
     */
    public void removeOptionsMenu() {
        menuId = 0;
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menuId != 0) {
            getMenuInflater().inflate(menuId, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public void hideInputtype() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

    }

    public void changeFont(String font) {
        // used for fonts library
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(font)
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        changeFont("fonts/JF-Flat-regular.ttf");
    }


    // add localization in all activities
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setLocale(String lang) {
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(lang)); // API 17+ only.
        res.updateConfiguration(conf, dm);
    }
}



