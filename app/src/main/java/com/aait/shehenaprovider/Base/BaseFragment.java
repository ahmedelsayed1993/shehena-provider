package com.aait.shehenaprovider.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.aait.shehenaprovider.Widget.Toaster;

import butterknife.ButterKnife;


/*
created by mahmoud 10/4/2017
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    public GlobalPreferences globalPreferences;
    public Bundle savedInstanceState;
    public Toaster toaster;
    protected CustomeProgressDialog customeProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getViewId(), container, false);
        globalPreferences = new GlobalPreferences(getActivity());
        this.savedInstanceState = savedInstanceState;
        toaster = new Toaster(getActivity());
        customeProgressDialog = new CustomeProgressDialog(getActivity(), "");
        ButterKnife.bind(this, view);
        init(view);

//        if (IsLogo()) {
//            MainActivity.tv_image.setVisibility(View.VISIBLE);
//        } else {
//            MainActivity.tv_image.setVisibility(View.GONE);
//        }

        if (SetTitle() != null) {
            MainActivity.tv_title.setText(SetTitle());
        } else {
        }
        return view;
    }


    protected abstract int getViewId();

    protected abstract void init(View view);

    protected abstract String SetTitle();

    // protected abstract boolean IsLogo();

}
