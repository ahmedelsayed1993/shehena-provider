package com.aait.shehenaprovider.Network;

/*
created by aya 16/8/2017
 */

import com.aait.shehenaprovider.Models.AboutModel.aboutModel;
import com.aait.shehenaprovider.Models.AcceptOrderModel.acceptOrderModel;
import com.aait.shehenaprovider.Models.AddAss;
import com.aait.shehenaprovider.Models.BankModel.bankModel;
import com.aait.shehenaprovider.Models.Categories.CategoriesHeadResponse;
import com.aait.shehenaprovider.Models.NewPassword.ChangePasswordModel;
import com.aait.shehenaprovider.Models.CurrentOrderModel.currentModel;
import com.aait.shehenaprovider.Models.FinishModel.finishModel;
import com.aait.shehenaprovider.Models.ForgetPasswordModel;
import com.aait.shehenaprovider.Models.LocationResponse.LocationResponse;
import com.aait.shehenaprovider.Models.LoginModel.loginModel;
import com.aait.shehenaprovider.Models.NewOrderModel.newOrdersModel;
import com.aait.shehenaprovider.Models.NewOrdersDetailsModel.newOrdersDetailsModel;
import com.aait.shehenaprovider.Models.NotificationModelServices.DeleteNotificationModel;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModel;
import com.aait.shehenaprovider.Models.PostCommissionModel;
import com.aait.shehenaprovider.Models.ProfileModel.profileModel;
import com.aait.shehenaprovider.Models.RefusedOrderModel;
import com.aait.shehenaprovider.Models.RegisterModel.registerModel;
import com.aait.shehenaprovider.Models.RestoreNewPassModel.restoreNewPassModel;
import com.aait.shehenaprovider.Models.SettingNotificationChangeResponse;
import com.aait.shehenaprovider.Models.SubCategories.SubCategoriesHeadResponse;
import com.aait.shehenaprovider.Models.TermsModel;
import com.aait.shehenaprovider.Models.UpdateProcessArrayModel.updateProcessArrModel;
import com.aait.shehenaprovider.Models.VerfyCodeModel;
import com.aait.shehenaprovider.Models.deleteAss;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ServiceApi {
  @GET(Urls.GETLOCATION)
  Call<LocationResponse> getLocation(
          @Query("latlng") String lng,
          @Query("language") String languge
  );
  @FormUrlEncoded
  @POST(Urls.terms)
  Call<TermsModel> getCondition(
          @Field(Urls.Keys.lang) String lang);

    @FormUrlEncoded
    @POST(Urls.about)
    Call<aboutModel> getAboutApp(
            @Field(Urls.Keys.lang) String lang);

    @FormUrlEncoded
    @POST(Urls.PROVIDER_EDIT_PROFILE)
    Call<profileModel> getProfile(
            @Field(Urls.Keys.lang) String lang,
            @Field(Urls.Keys.provider_id) String provider_id);

    @FormUrlEncoded
    @POST(Urls.LOGIN)
    Call<loginModel> LOGIN(
            @Field(Urls.Keys.phone) String phone,
            @Field(Urls.Keys.password) String password,
            @Field(Urls.Keys.device_id) String device_id);

    @FormUrlEncoded
    @POST(Urls.REGISTER)
    Call<registerModel> getRegister(
            @Field(Urls.Keys.name) String name,
            @Field(Urls.Keys.email) String email,
            @Field(Urls.Keys.password) String password,
            @Field(Urls.Keys.address) String address,
            @Field(Urls.Keys.lat) String lat,
            @Field(Urls.Keys.lng) String lng,
            @Field(Urls.Keys.phone) String phone,
            @Field(Urls.Keys.car_num) String car_num,
            @Field(Urls.Keys.identity_num) String identity_num,
            @Field(Urls.Keys.price) String price,
            @Field(Urls.Keys.sub_category_id) String sub_category_id,
            @Field(Urls.Keys.avatar) String avatar,
            @Field(Urls.Keys.Insurance_file) String Insurance_file,
            @Field(Urls.Keys.identity_image) String identity_image,
            @Field(Urls.Keys.assistants) String assistants,
            @Field(Urls.Keys.device_id) String device_id,
            @Field(Urls.Keys.car_size) String size,
            @Field(Urls.Keys.notes) String notes
    );

  @FormUrlEncoded
  @POST(Urls.ADD_REFUSED)
  Call<RefusedOrderModel> getRefusedOrder(
          @Field(Urls.Keys.user_id) String provider_id,
          @Field(Urls.Keys.order_id) String order_id,
          @Field(Urls.Keys.reason) String reason,
          @Field(Urls.Keys.notes) String notes
  );
  @FormUrlEncoded
  @POST(Urls.settings)
  Call<SettingNotificationChangeResponse> settings(
          @Field(Urls.Keys.user_id) String user_id,
          @Field(Urls.Keys.lang) String lang,
          @Field(Urls.Keys.notification) String notification
  );
  @FormUrlEncoded
  @POST(Urls.UPDATE_PROFILE)
  Call<profileModel> getUpdate(
          @Field(Urls.Keys.provider_id) String provider_id,
          @Field(Urls.Keys.name) String name,
          @Field(Urls.Keys.email) String email,
          @Field(Urls.Keys.address) String address,
          @Field(Urls.Keys.lat) String lat,
          @Field(Urls.Keys.lng) String lng,
          @Field(Urls.Keys.phone) String phone,
          @Field(Urls.Keys.car_num) String car_num,
          @Field(Urls.Keys.identity_num) String identity_num,
          @Field(Urls.Keys.price) String price,
          @Field(Urls.Keys.sub_category_id) String sub_category_id,
          @Field(Urls.Keys.identity_image) String identity_image,
          @Field(Urls.Keys.Insurance_file) String Insurance_file,
          @Field(Urls.Keys.avatar) String avatar,
          @Field(Urls.Keys.car_size) String car_size,
          @Field(Urls.Keys.notes) String notes);


    @FormUrlEncoded
    @POST(Urls.CATEGORIES)
    Call<CategoriesHeadResponse> getCategory(
            @Field(Urls.Keys.lang) String lang);

  @FormUrlEncoded
  @POST(Urls.ACCEPT_ORDER)
  Call<acceptOrderModel> getAcceptOrder(
          @Field(Urls.Keys.order_id) String order_id);

    @FormUrlEncoded
    @POST(Urls.SUB_CATEGORIES)
    Call<SubCategoriesHeadResponse> getSubCategory(
            @Field(Urls.Keys.lang) String lang,
            @Field(Urls.Keys.category_id) String category_id);

    @FormUrlEncoded
    @POST(Urls.BANK)
    Call<bankModel> getBanks(
            @Field(Urls.Keys.lang) String lang,
            @Field(Urls.Keys.provider_id) String provider_id);

  @FormUrlEncoded
  @POST(Urls.NEW_ORDER_DETAILS)
  Call<newOrdersDetailsModel> getOrderDetails(
          @Field(Urls.Keys.order_id) String order_id,
          @Field(Urls.Keys.lang) String lang);
    @FormUrlEncoded
    @POST(Urls.FINISH_ORDERS)
    Call<finishModel> getFinishOrders(
            @Field(Urls.Keys.provider_id) String provider_id,
            @Field(Urls.Keys.lang) String lang);

    @FormUrlEncoded
    @POST(Urls.CURRENT_ORDERS)
    Call<currentModel> getCurrentOrders(
            @Field(Urls.Keys.provider_id) String provider_id,
            @Field(Urls.Keys.lang) String lang);
  @FormUrlEncoded
  @POST(Urls.Remove_Ass)
  Call<deleteAss> getRemoveAss(
          @Field(Urls.Keys.id) String id);

    @FormUrlEncoded
    @POST(Urls.NEW_ORDERS)
    Call<newOrdersModel> getNewOrders(
            @Field(Urls.Keys.provider_id) String provider_id,
            @Field(Urls.Keys.lang) String lang);

  @FormUrlEncoded
  @POST(Urls.NEW_PASSWORD)
  Call<ChangePasswordModel> getNewPassword(
          @Field(Urls.Keys.user_id) String user_id,
          @Field(Urls.Keys.new_password) String new_password);
  @FormUrlEncoded
  @POST(Urls.POST_COMMISSION)
  Call<PostCommissionModel> getCommission(
          @Field(Urls.Keys.user_id) String user_id,
          @Field(Urls.Keys.from) String from,
          @Field(Urls.Keys.to) String to,
          @Field(Urls.Keys.value) String value,
          @Field(Urls.Keys.account_name) String account_name
          ,@Field(Urls.Keys.lang) String lang);


  @FormUrlEncoded
  @POST(Urls.Add_Ass)
  Call<AddAss> getAddAss(
          @Field(Urls.Keys.provider_id) String provider_id,
          @Field(Urls.Keys.assistant_name) String assistant_name,
          @Field(Urls.Keys.assistants_job) String assistants_job);

  @FormUrlEncoded
  @POST(Urls.FORGET_PASSWORD)
  Call<ForgetPasswordModel> getForgetPassword(
          @Field(Urls.Keys.phone) String phone
          ,@Field(Urls.Keys.lang) String lang);
  @FormUrlEncoded
  @POST(Urls.Verify_Code)
  Call<VerfyCodeModel> getCode(
          @Field(Urls.Keys.code) String code);

  @FormUrlEncoded
  @POST(Urls.BRAND_NEW_PASSWORD)
  Call<restoreNewPassModel> getBrandNewPassword(
          @Field(Urls.Keys.phone) String phone,
          @Field(Urls.Keys.newpassword) String newpassword);

  @FormUrlEncoded
  @POST(Urls.NOTIFICATIONS)
  Call<NotificationModel> getNotification(
          @Field(Urls.Keys.user_id) String user_id,
          @Field(Urls.Keys.lang) String lang);

  @FormUrlEncoded
  @POST(Urls.DELETE_NOTIFICATIONS)
  Call<DeleteNotificationModel> getDeleteNotification(
          @Field(Urls.Keys.noti_id) String noti_id,
          @Field(Urls.Keys.lang) String lang);

  @FormUrlEncoded
  @POST(Urls.UPDATE_ORDER_PROCESS)
  Call<updateProcessArrModel> getUpdateProcessArray(
          @Field(Urls.Keys.order_id) String order_id);
}
