package com.aait.shehenaprovider.Network;
/*
created by mahmoud 16/4/2017
 */

public class Urls {

    public final static String ENDPOINT = "http://hamada.arabsdesign.com/shahna/api/";
    public final static String HTTTPIMG = "http://45.77.1.233/public/uploads/social/";
    public final static String about = "about-app";
    public final static String terms = "terms";
    public final static String GETLOCATION = "geocode/json?sensor=true";

    public final static String LOGIN = "login";
    public final static String CATEGORIES = "categories";
    public final static String SUB_CATEGORIES = "sub-category";
    public final static String REGISTER = "register-provider";
    public final static String UPDATE_PROFILE = "provider-update-profile";

    public final static String BANK = "bank";
    public final static String FINISH_ORDERS = "finish-orders-provider";
    public final static String NEW_ORDERS = "new-orders-provider";
    public final static String CURRENT_ORDERS = "current-orders-provider";
    public final static String PROVIDER_EDIT_PROFILE = "provider-edit-profile";
    public final static String NEW_ORDER_DETAILS = "new-order-details";
    public final static String ADD_REFUSED = "add-refused";
    public final static String ACCEPT_ORDER = "accept-order";
    public final static String UPDATE_ORDER_PROCESS = "update-order-process";
    public final static String NEW_PASSWORD = "new-password";
    public final static String POST_COMMISSION = "post-commission";
    public final static String FORGET_PASSWORD = "forget-password";
    public final static String BRAND_NEW_PASSWORD = "brand-new-password";
    public final static String NOTIFICATIONS = "notifications";
    public final static String DELETE_NOTIFICATIONS = "delete-notification";
    public final static String settings = "settings";
    public final static String Remove_Ass = "remove-assistant";
    public final static String Add_Ass = "add-assistant";
    public final static String Verify_Code = "verify-code";

    /*****  services keys *****/
    public static class Keys {
        public final static String code = "code";

        public final static String lang = "lang";
        public final static String notification = "notification";
        public final static String phone = "phone";
        public final static String password = "password";
        public final static String category_id = "category_id";
        public final static String name = "name";
        public final static String email = "email";
        public final static String address = "address";
        public final static String lat = "lat";
        public final static String lng = "lng";
        public final static String car_num = "car_num";
        public final static String identity_num = "identity_num";
        public final static String price = "price";
        public final static String sub_category_id = "sub_category_id";
        public final static String identity_image = "identity_image";
        public final static String Insurance_file = "Insurance_file";
        public final static String avatar = "avatar";
        public final static String assistant_name = "assistant_name[]";
        public final static String assistants_job = "assistants_job[]";
        public final static String assistants = "assistants";

        public final static String car_size = "car_size";

        public final static String provider_id = "provider_id";
        public final static String type = "type";
        public final static String order_id = "order_id";
        public final static String date = "date";
        public final static String device_id = "device_id";
        public final static String time = "time";
        public final static String payment_type = "payment_type";
        public final static String id = "id";

        public final static String user_id = "user_id";
        public final static String reason = "reason";
        public final static String notes = "note";
        public final static String old_password = "old_password";
        public final static String new_password = "new_password";
        public final static String from = "account_num_from";
        public final static String to = "account_num_to";
        public final static String value = "ammount";
        public final static String account_name = "account_owner_name";
        public final static String newpassword = "newpassword";
        public final static String noti_id = "noti_id";


    }


}
