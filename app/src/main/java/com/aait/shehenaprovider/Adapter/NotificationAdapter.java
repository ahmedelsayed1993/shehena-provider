package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Fragment.NewOrdersFragment;
import com.aait.shehenaprovider.Listners.INotificationFragment;
import com.aait.shehenaprovider.Models.NotificationModelServices.DeleteNotificationModel;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModelData;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.Toaster;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by aya on 2/7/2017.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    public static String TAG = NotificationAdapter.class.getSimpleName();

    public static Context context;
    private int lastPosition = -1;
    ArrayList<NotificationModelData> arrayList;
    INotificationFragment iNotificationFragment;
    Toaster toaster;
    GlobalPreferences globalPreferences;

    public NotificationAdapter(Context context,INotificationFragment iNotificationFragment) {
        this.context = context;
        this.iNotificationFragment = iNotificationFragment;
        arrayList = new ArrayList<>();
    }

    public void addNotification(NotificationModelData d){
        arrayList.add(d);
    }

    public void clearNotifications(){
        if (arrayList != null) {
            if (arrayList.size() != 0) {
                arrayList.clear();
            }
        }
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_notifications, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        holder.tv_noti.setText(arrayList.get(position).getMessage());
        holder.iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int p = holder.getAdapterPosition();
                //comment
                if (Util.isNetworkAvailable(context)) {
                    RetroWeb.getClient().create(ServiceApi.class).getDeleteNotification(String.valueOf(arrayList.get(p).getId()),globalPreferences.getLang()).enqueue(new Callback<DeleteNotificationModel>() {
                        @Override
                        public void onResponse(Call<DeleteNotificationModel> call, Response<DeleteNotificationModel> response) {
                            if (response.body().getStatus().equals("1")){
                                toaster.makeToast(response.body().getMessage());
                                arrayList.remove(position);
                                notifyDataSetChanged();
                                MainActivity.notification_count.setText(arrayList.size()+"");
                               // iNotificationFragment.OnRefreshNotificationAdapter();
                            }else {
                                toaster.makeToast(response.body().getMessage());
//                                toaster.makeToast("Response Null");
                                Log.i("NullResponse","deleteNotification in NotificationAdapter");
                            }
                        }

                        @Override
                        public void onFailure(Call<DeleteNotificationModel> call, Throwable t) {
                            Log.i("OnFailure","deleteNotification in NotificationAdapter");
//                            swiperefresh.setRefreshing(false);
//                            lay_progress.setVisibility(View.GONE);
                        }
                    });
                } else {
                    toaster.makeToast(context.getString(R.string.NoInternetConnection));

                }
            }
        });



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(NewOrdersFragment.class.getName()).replace(R.id.content, new NewOrdersFragment()).commit();

                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(NewOrdersFragment.class.getName()).replace(R.id.content, new NewOrdersFragment()).commit();
            }
        });

        setAnimation(holder.itemView, position);

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_close;
        TextView tv_noti;
        public ViewHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            iv_close = (ImageView) itemView.findViewById(R.id.iv_close);
            tv_noti = (TextView) itemView.findViewById(R.id.tv_notification);

        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
