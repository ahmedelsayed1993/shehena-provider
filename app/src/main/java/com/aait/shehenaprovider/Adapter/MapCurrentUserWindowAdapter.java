package com.aait.shehenaprovider.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import com.aait.shehenaprovider.Components.LitterAsImage;
import com.aait.shehenaprovider.Models.CurrentOrderModel.CurrentData;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mahmooud on 18/09/2016.
 */
public class MapCurrentUserWindowAdapter implements GoogleMap.InfoWindowAdapter{

    Activity context;
    View view;
    TextView tv_name, tv_more;
    public CurrentData model;
    CircleImageView user_image;


    public MapCurrentUserWindowAdapter(Activity context, CurrentData model) {
        this.context = context;
        this.model = model;

    }


    @Override
    public View getInfoContents(Marker marker) {
        return null;

    }
    @Override
    public View getInfoWindow(Marker marker) {
        // Getting view from the layout file
        View view = context.getLayoutInflater().inflate(R.layout.user_window_shahena_layout, null);
        init(view);
        return view;
    }

    private void init(View view) {
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        user_image = (CircleImageView) view.findViewById(R.id.user_image);
        tv_more = (TextView) view.findViewById(R.id.tv_more);
           Util.onPrintLog(model);

            tv_name.setText(model.getUser_name());
        LitterAsImage litterAsImage = new LitterAsImage(context);
        Bitmap bitmap = litterAsImage.litterToImage(R.drawable.oval_colorprimary, model.getUser_name());
        Drawable d = new BitmapDrawable(context.getResources(), bitmap);

        /*if (!model.getAvatar().equals("")) {
            Picasso.with(context).load(model.getAvatar()).placeholder(d).resize(200,200).into(user_image);

        } else {

            user_image.setImageBitmap(bitmap);
        }*/

    }


}
