package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Models.AboutModel.AllSocial;
import com.aait.shehenaprovider.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 7/31/2017.
 */

public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.ViewHolder> {
    public static String TAG = AboutAdapter.class.getSimpleName();

    public static Context context;
    List<AllSocial> Models;
    private int lastPosition = -1;
    String url="";


    public AboutAdapter(Context context, List<AllSocial> Models) {
        this.context = context;
        this.Models = Models;
    }

    @Override
    public AboutAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_about_social, parent, false);
        AboutAdapter.ViewHolder viewHolder = new AboutAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AboutAdapter.ViewHolder holder,  int position) {
        final AllSocial model=Models.get(position);

        holder.ib_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (model.getSocial_name().equals("twitter"))
                    {
                        openURL(model.getSocial_url());
                    }
               else if (model.getSocial_name().equals("faceboob"))
                {
                    openURL(model.getSocial_url());
                }
               else if (model.getSocial_name().equals("instagram"))
                {
                    openURL(model.getSocial_url());
                }
               else if (model.getSocial_name().equals("youtube"))
                {
                    openURL(model.getSocial_url());
                }
                    else if (model.getSocial_name().equals("google +"))
                    {
                        openURL(model.getSocial_url());
                    }
            }
        });

        Picasso.with(context).load(model.getSocial_image()).into(holder.ib_image);
       // setAnimation(holder.itemView, position);

    }
    private void openURL(String url) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            MainActivity.activity.startActivity(browserIntent);
        } catch (Exception e) {
            Log.e("Exption", e.toString());
        }

    }

    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ib_image)
        ImageButton ib_image;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }


}
