package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.shehenaprovider.Models.AssistantsModel2;
import com.aait.shehenaprovider.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssistancesAdapter2 extends RecyclerView.Adapter<AssistancesAdapter2.ViewHolder> {


    ArrayList<AssistantsModel2> assistantsArrayList;

    public AssistancesAdapter2(Context context){

        assistantsArrayList = new ArrayList<>();
    }

    public void addAssistance(AssistantsModel2 a){
        assistantsArrayList.add(a);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_helper, parent, false);
        AssistancesAdapter2.ViewHolder viewHolder = new AssistancesAdapter2.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_help_name.setText(assistantsArrayList.get(position).getName());
        holder.tv_help_job.setText(assistantsArrayList.get(position).getJob());
        holder.et_count.setText(assistantsArrayList.get(position).getCount());
    }

    @Override
    public int getItemCount() {
        return assistantsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_help_name)
        TextView tv_help_name;
        @BindView(R.id.tv_help_job)
        TextView tv_help_job;
        @BindView(R.id.tv_count)
        TextView et_count;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

