package com.aait.shehenaprovider.Adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Listners.NavigationActionListner;
import com.aait.shehenaprovider.Models.MenuModel;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.aait.shehenaprovider.Widget.Toaster;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by mahmoud on 12/1/2017.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    AppCompatActivity context;
    ArrayList<MenuModel> navigationItem;
    GlobalPreferences globalPreferences;
    NavigationActionListner navigationActionListner;
    Toaster toaster;

    public NavigationDrawerAdapter(AppCompatActivity context, ArrayList<MenuModel> navigationItem, NavigationActionListner navigationActionListner) {
        this.context = context;
        this.navigationItem = navigationItem;
        globalPreferences = new GlobalPreferences(context);
        this.navigationActionListner = navigationActionListner;
    }

    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_menu_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder holder, final int position) {
        toaster = new Toaster(context);
        holder.tv_nav_name.setText(navigationItem.get(position).getName());
        Picasso.with(context).load(navigationItem.get(position).getImage_id()).into(holder.iv_nav_pic);
//        if (!navigationItem.get(position).getNumber().equals("")) {
//            holder.tv_number.setVisibility(View.VISIBLE);
//            holder.tv_number.setText("" + navigationItem.get(position).getNumber());
//        }
        if (navigationItem.get(position).getNumber()!=null&&
                !navigationItem.get(position).getNumber().equals("")) {
            holder.tv_number.setText(navigationItem.get(position).getNumber());
            holder.tv_number.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    navigationActionListner.onProfile();
                } else if (position == 1) {
                    navigationActionListner.onNotification();
                } else if (position == 2) {
                    navigationActionListner.onRequestes();
                } else if (position == 3) {
                    navigationActionListner.onNewRequestes();
                } else if (position == 4) {
                    navigationActionListner.onFinishRequestes();
                } else if (position == 5) {
                    navigationActionListner.onAccounts();
                } else if (position == 6) {
                    navigationActionListner.onSettings();
                } else if (position == 7) {
                    navigationActionListner.onAbout();
                } else if (position == 8) {
                    navigationActionListner.onShare();
                } else if (position == 9) {
                    navigationActionListner.onLogout();
                }
                drawerOpenClose();
            }
        });
    }


    @Override
    public int getItemCount() {
        return navigationItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nav_name;
        ImageView iv_nav_pic;
        TextView tv_number;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_nav_name = (TextView) itemView.findViewById(R.id.tv_nav_name);
            iv_nav_pic = (ImageView) itemView.findViewById(R.id.iv_nav_pic);
            tv_number = (TextView) itemView.findViewById(R.id.tv_number);

        }
    }


    public void drawerOpenClose() {
        if (globalPreferences.getLang().equals("ar")) {
            MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            MainActivity.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }
}
