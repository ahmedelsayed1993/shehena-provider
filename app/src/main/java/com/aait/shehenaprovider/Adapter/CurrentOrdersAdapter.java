package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.aait.shehenaprovider.Activities.MainActivity;
import com.aait.shehenaprovider.Fragment.Map;
import com.aait.shehenaprovider.Models.CurrentOrderModel.CurrentData;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 7/31/2017.
 */

public class CurrentOrdersAdapter extends RecyclerView.Adapter<CurrentOrdersAdapter.ViewHolder> {
    public static String TAG = CurrentOrdersAdapter.class.getSimpleName();

    public static Context context;
    List<CurrentData> Models;
    private int lastPosition = -1;


    public CurrentOrdersAdapter(Context context, List<CurrentData> Models) {
        this.context = context;
        this.Models = Models;
    }

    @Override
    public CurrentOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_requests, parent, false);
        CurrentOrdersAdapter.ViewHolder viewHolder = new CurrentOrdersAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CurrentData model=Models.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CurrentData currentOrder_data=model;
                Util.onPrintLog(currentOrder_data);
                Bundle bundle = new Bundle();
                bundle.putString("id",""+model.getOrder_id());
                bundle.putString("page","current");
                bundle.putSerializable("model",model);
                bundle.putString("service",model.getService_name());

                Map fragment = new Map();
                fragment.setArguments(bundle);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(Map.class.getName()).replace(R.id.content, fragment).commit();
            }
        });
        holder.tv_name.setText(model.getUser_name());
        holder.tv_service_type.setText(model.getService_name());
        String payment=model.getPayment_type();
        if (payment.equals("cash"))
            holder.tv_payment_method.setText(context.getResources().getString(R.string.cash));
        else
            holder.tv_payment_method.setText(context.getResources().getString(R.string.online));
        holder.tv_price.setText(model.getPrice()+" "+ context.getResources().getString(R.string.reyal));
        holder.tv_start_date.setText(model.getDate());
        if (model.getOrder_processing()==0)
            holder.tv_state.setText(context.getResources().getString(R.string.wait));
       else if (model.getOrder_processing()==1)
            holder.tv_state.setText(context.getResources().getString(R.string.the_accept));
        else if (model.getOrder_processing()==2)
            holder.tv_state.setText(context.getResources().getString(R.string.on_away));
        else if (model.getOrder_processing()==3)
            holder.tv_state.setText(context.getResources().getString(R.string.arrival));
        else if (model.getOrder_processing()==4)
            holder.tv_state.setText(context.getResources().getString(R.string.start_work));
        else if (model.getOrder_processing()==5)
            holder.tv_state.setText(context.getResources().getString(R.string.end));
        else if (model.getOrder_processing()==6)
            holder.tv_state.setText(context.getResources().getString(R.string.complete));
        else if (model.getOrder_processing()==7)
            holder.tv_state.setText(context.getResources().getString(R.string.canceled));

        holder.tv_request_num.setText(model.getOrder_num()+"");
        holder.tv_time.setText(model.getTime());
        holder.tv_phone.setText(model.getUser_phone());
        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_service_type)
        TextView tv_service_type;
        @BindView(R.id.tv_payment_method)
        TextView tv_payment_method;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_start_date)
        TextView tv_start_date;
        @BindView(R.id.tv_state)
        TextView tv_state;
        @BindView(R.id.tv_request_num)
        TextView tv_request_num;
        @BindView(R.id.tv_time)
        TextView tv_time;

        @BindView(R.id.tv_phone)
        TextView tv_phone;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }


}