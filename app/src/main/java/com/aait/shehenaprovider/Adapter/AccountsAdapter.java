package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.aait.shehenaprovider.Models.BankModel.BanksData;
import com.aait.shehenaprovider.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 7/31/2017.
 */

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.ViewHolder> {
    public static String TAG = AccountsAdapter.class.getSimpleName();

    public static Context context;
    List<BanksData> Models;
    private int lastPosition = -1;


    public AccountsAdapter(Context context,List<BanksData> Models) {
        this.context = context;
        this.Models = Models;
    }

    @Override
    public AccountsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_accounts, parent, false);
        AccountsAdapter.ViewHolder viewHolder = new AccountsAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AccountsAdapter.ViewHolder holder, int position) {
        BanksData model=Models.get(position);
        holder.tv_bank_name.setText(model.getBank_name_en());
        holder.tv_bank_number.setText(model.getBank_number());
        setAnimation(holder.itemView, position);
    }


    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_bank_number)
        TextView tv_bank_number;
        @BindView(R.id.tv_bank_name)
        TextView tv_bank_name;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }


}
