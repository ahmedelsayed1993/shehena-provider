package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.aait.shehenaprovider.Models.FinishModel.FinishData;
import com.aait.shehenaprovider.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 7/31/2017.
 */

public class FinishOrdersAdapter extends RecyclerView.Adapter<FinishOrdersAdapter.ViewHolder> {
    public static String TAG = FinishOrdersAdapter.class.getSimpleName();

    public static Context context;
    List<FinishData> Models;
    private int lastPosition = -1;


    public FinishOrdersAdapter(Context context, List<FinishData> Models) {
        this.context = context;
        this.Models = Models;
    }

    @Override
    public FinishOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_finish_requests, parent, false);
        FinishOrdersAdapter.ViewHolder viewHolder = new FinishOrdersAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        FinishData model=Models.get(position);
        holder.tv_name.setText(model.getUser_name());
        holder.tv_service_type.setText(model.getService_name());
        String payment=model.getPayment_type();
        if (payment.equals("cash"))
            holder.tv_payment_method.setText(context.getResources().getString(R.string.cash));
        else
            holder.tv_payment_method.setText(context.getResources().getString(R.string.online));

        holder.tv_price.setText(model.getPrice()+" "+context.getResources().getString(R.string.reyal));
        holder.tv_start_date.setText(model.getDate());
        if (model.getOrder_processing()==6){
            holder.tv_state.setText(context.getString(R.string.complete));
        }
        else {holder.tv_state.setText(context.getString(R.string.canceled));}

        holder.tv_request_num.setText(model.getOrder_num()+"");
        holder.tv_time.setText(model.getTime());
        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_service_type)
        TextView tv_service_type;
        @BindView(R.id.tv_payment_method)
        TextView tv_payment_method;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_start_date)
        TextView tv_start_date;
        @BindView(R.id.tv_state)
        TextView tv_state;
        @BindView(R.id.tv_request_num)
        TextView tv_request_num;
        @BindView(R.id.tv_time)
        TextView tv_time;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }


}