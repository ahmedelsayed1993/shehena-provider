package com.aait.shehenaprovider.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Listners.IAssistantFragment;
import com.aait.shehenaprovider.Models.ProfileModel.Assistants;
import com.aait.shehenaprovider.Models.deleteAss;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.Toaster;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by aya on 2/7/2017.
 */
public class AssistantAdapter extends RecyclerView.Adapter<AssistantAdapter.ViewHolder> {
    public static String TAG = AssistantAdapter.class.getSimpleName();

    public static Context context;
    ArrayList<Assistants> Models;
    private int lastPosition = -1;
    IAssistantFragment iNotificationFragment;
    Toaster toaster;
    GlobalPreferences globalPreferences;


    public AssistantAdapter(Context context, IAssistantFragment iNotificationFragment) {
        this.context = context;
        Models =  new ArrayList<>();
        this.iNotificationFragment = iNotificationFragment;

    }
    public void addItem(Assistants m){
        Models.add(m);
    }
    public void clearArray(){
        if (Models.size() > 0) {
            Models.clear();
        }
    }

    @Override
    public AssistantAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_assistant, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        final Assistants model=Models.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Bundle bundle = new Bundle();
                bundle.putString("id",""+model.getId());
                if (model.getType().equals("newOrder")) {
                    bundle.putString("page", "new");
                }
                Map fragment = new Map();
                fragment.setArguments(bundle);*/
               // MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(NewOrdersFragment.class.getName()).replace(R.id.content, new NewOrdersFragment()).commit();
            }
        });
        holder.tv_help.setText(model.getName());
        holder.tv_job.setText(model.getJob());

        setAnimation(holder.itemView, position);

        holder.iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteAss(model.getId()+"");
            }
        });
    }
    public void DeleteAss(String Ass_id) {
        Log.e(TAG, "Notification Id :" + Ass_id);
        Log.i("NOTI_id",Ass_id);
        RetroWeb.getClient().create(ServiceApi.class).getRemoveAss(Ass_id).enqueue(new Callback<deleteAss>() {
            @Override
            public void onResponse(Call<deleteAss> call, Response<deleteAss> response) {
                Util.onPrintLog(response.body());
                Log.e("value",response.body().getStatus());
                if (response.body().getStatus().equals("1")) {
                   // toaster.makeToast(response.body().getMessage());
                    iNotificationFragment.OnRefreshAssAdapter();

                }else {
                    toaster.makeToast("Failed to remove assistant");
                }
            }

            @Override
            public void onFailure(Call<deleteAss> call, Throwable t) {
                Util.handleException(context, t);
                t.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (Models != null) {
            return Models.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_help)
        TextView tv_help;
        @BindView(R.id.tv_job)
        TextView tv_job;
        @BindView(R.id.iv_close)
        ImageView iv_close;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
