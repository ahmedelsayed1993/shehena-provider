package com.aait.shehenaprovider.Models.NewOrderModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aya on 9/20/2017.
 */

public class newOrdersModel implements Serializable{
    @SerializedName("data")
    private List<NewOrderData> data;

    public List<NewOrderData> getData() {
        return data;
    }

    public void setData(List<NewOrderData> data) {
        this.data = data;
    }
}
