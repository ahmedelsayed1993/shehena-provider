package com.aait.shehenaprovider.Models.Categories;

/**
 * Created by agamidev on 9/13/17.
 */

public class CategoriesDataResponse {
        /**
         * id : 11
         * name_ar : لللل
         * logo : public/uploads/sections_icon/10-08-171502353986129155165.jpeg
         */

    private int id;
    private String logo;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
            return logo;
        }

    public void setLogo(String logo) {
            this.logo = logo;
        }
}
