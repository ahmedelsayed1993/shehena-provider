package com.aait.shehenaprovider.Models.AcceptOrderModel;

import com.aait.shehenaprovider.Models.Process_array;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aya on 9/12/2017.
 */

public class acceptOrderModel implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("process_array")
    private List<Process_array> process_array;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Process_array> getProcess_array() {
        return process_array;
    }

    public void setProcess_array(List<Process_array> process_array) {
        this.process_array = process_array;
    }
}
