
package com.aait.shehenaprovider.Models;

import java.io.Serializable;

/*
created by mahmoud 10/4/2017
 */
public class UserModel implements Serializable{
    /**
     * id : 16
     * name : mahmoudd
     * email :
     * phochgchgcto :
     * phone : 01092565559
     * points : 0
     * code :
     * hot_line : 55555
     */

    private String id;
    private String name;
    private String email;
    private String photo;
    private String phone;
    private int points;
    private String code;
    private String hot_line;
    private  double lat;
    private  double lng;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHot_line() {
        return hot_line;
    }

    public void setHot_line(String hot_line) {
        this.hot_line = hot_line;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
