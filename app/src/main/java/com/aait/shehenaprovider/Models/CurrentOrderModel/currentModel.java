package com.aait.shehenaprovider.Models.CurrentOrderModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/8/2017.
 */

public class currentModel {


    @SerializedName("data")
    private List<CurrentData> data;

    public List<CurrentData> getData() {
        return data;
    }

    public void setData(List<CurrentData> data) {
        this.data = data;
    }
}
