package com.aait.shehenaprovider.Models.RestoreNewPassModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 9/19/2017.
 */

public class restoreNewPassModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private RestoreNewPassData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public RestoreNewPassData getData() {
        return data;
    }

    public void setData(RestoreNewPassData data) {
        this.data = data;
    }
}
