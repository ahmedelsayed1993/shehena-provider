package com.aait.shehenaprovider.Models.FinishModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/8/2017.
 */

public class finishModel {
    @SerializedName("data")
    private List<FinishData> data;

    public List<FinishData> getData() {
        return data;
    }

    public void setData(List<FinishData> data) {
        this.data = data;
    }
}
