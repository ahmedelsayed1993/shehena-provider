package com.aait.shehenaprovider.Models.BankModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("commission")
    private double commission;
    @SerializedName("banks")
    private List<BanksData> banks;

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public List<BanksData> getBanks() {
        return banks;
    }

    public void setBanks(List<BanksData> banks) {
        this.banks = banks;
    }
}
