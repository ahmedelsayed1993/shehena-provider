package com.aait.shehenaprovider.Models.NewOrdersDetailsModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 9/7/2017.
 */

public class newOrdersDetailsModel {

    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private NewOrderDetailsData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NewOrderDetailsData getData() {
        return data;
    }

    public void setData(NewOrderDetailsData data) {
        this.data = data;
    }
}
