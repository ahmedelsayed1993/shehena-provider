package com.aait.shehenaprovider.Models.RegisterModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterData {
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("address")
    private String address;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("phone")
    private String phone;
    @SerializedName("identity_num")
    private String identity_num;
    @SerializedName("car_num")
    private String car_num;
    @SerializedName("price")
    private String price;
    @SerializedName("sub_category_id")
    private String sub_category_id;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("identity_image")
    private String identity_image;
    @SerializedName("Insurance_file")
    private String Insurance_file;
    @SerializedName("assistant")
    private List<AssistantNameModel> assistant;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentity_num() {
        return identity_num;
    }

    public void setIdentity_num(String identity_num) {
        this.identity_num = identity_num;
    }

    public String getCar_num() {
        return car_num;
    }

    public void setCar_num(String car_num) {
        this.car_num = car_num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIdentity_image() {
        return identity_image;
    }

    public void setIdentity_image(String identity_image) {
        this.identity_image = identity_image;
    }

    public String getInsurance_file() {
        return Insurance_file;
    }

    public void setInsurance_file(String Insurance_file) {
        this.Insurance_file = Insurance_file;
    }

    public List<AssistantNameModel> getAssistant() {
        return assistant;
    }

    public void setAssistant(List<AssistantNameModel> assistant) {
        this.assistant = assistant;
    }
}
