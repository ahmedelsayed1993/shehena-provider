package com.aait.shehenaprovider.Models.CountryModel;

import java.util.List;

/**
 * Created by Aya on 8/27/2017.
 */

public class Person {
    private String name;
    private List<modelt> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<modelt> getData() {
        return data;
    }

    public void setData(List<modelt> data) {
        this.data = data;
    }
}
