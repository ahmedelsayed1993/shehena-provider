package com.aait.shehenaprovider.Models.NewOrdersDetailsModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User_data implements Serializable{
    @SerializedName("username")
    private String username;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
