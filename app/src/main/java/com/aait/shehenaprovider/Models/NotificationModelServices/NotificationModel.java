
package com.aait.shehenaprovider.Models.NotificationModelServices;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NotificationModel implements Serializable {


    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private List<NotificationModelData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<NotificationModelData> getData() {
        return data;
    }

    public void setData(List<NotificationModelData> data) {
        this.data = data;
    }
}
