package com.aait.shehenaprovider.Models.ProfileModel;

import com.google.gson.annotations.SerializedName;

public class Assistants {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("job")
    private String job;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
