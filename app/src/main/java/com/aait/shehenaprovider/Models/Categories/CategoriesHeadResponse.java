package com.aait.shehenaprovider.Models.Categories;

import java.util.ArrayList;

/**
 * Created by agamidev on 9/13/17.
 */

public class CategoriesHeadResponse {


    private ArrayList<CategoriesDataResponse> data;

    public ArrayList<CategoriesDataResponse> getData() {
        return data;
    }

    public void setData(ArrayList<CategoriesDataResponse> data) {
        this.data = data;
    }

}
