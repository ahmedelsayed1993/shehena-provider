package com.aait.shehenaprovider.Models.ProfileModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 9/5/2017.
 */

public class profileModel {


    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private Data data;
    @SerializedName("value")
    private String value;
    @SerializedName("msg")
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
