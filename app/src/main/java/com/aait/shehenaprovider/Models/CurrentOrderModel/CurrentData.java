package com.aait.shehenaprovider.Models.CurrentOrderModel;

import com.aait.shehenaprovider.Models.Process_array;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CurrentData implements Serializable {
    @SerializedName("order_id")
    private int order_id;
    @SerializedName("status")
    private int status;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("user_phone")
    private String user_phone;
    @SerializedName("order_num")
    private int order_num;
    @SerializedName("payment_type")
    private String payment_type;
    @SerializedName("assistants")
    private String assistants;
    @SerializedName("price")
    private String price;
    @SerializedName("date")
    private String date;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("service_name")
    private String service_name;
    @SerializedName("order_processing")
    private int order_processing;
    @SerializedName("process_array")
    private List<Process_array> process_array;
    @SerializedName("time")
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getOrder_num() {
        return order_num;
    }

    public void setOrder_num(int order_num) {
        this.order_num = order_num;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getAssistants() {
        return assistants;
    }

    public void setAssistants(String assistants) {
        this.assistants = assistants;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getOrder_processing() {
        return order_processing;
    }

    public void setOrder_processing(int order_processing) {
        this.order_processing = order_processing;
    }

    public List<Process_array> getProcess_array() {
        return process_array;
    }

    public void setProcess_array(List<Process_array> process_array) {
        this.process_array = process_array;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }
}
