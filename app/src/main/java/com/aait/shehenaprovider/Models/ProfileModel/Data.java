package com.aait.shehenaprovider.Models.ProfileModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("provider_id")
    private int provider_id;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("address")
    private String address;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("identity_num")
    private String identity_num;
    @SerializedName("car_num")
    private String car_num;
    @SerializedName("price")
    private String price;
    @SerializedName("subcategory_id ")
    private int subcategory_id;
    @SerializedName("category_id ")
    private int category_id;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    @SerializedName("service_name")
    private String service_name;
    @SerializedName("rating")
    private int rating;
    @SerializedName("successOrders")
    private int successOrders;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("identity_image")
    private String identity_image;
    @SerializedName("Insurance_file")
    private String Insurance_file;
    @SerializedName("assistants")
    private List<Assistants> assistants;


    @SerializedName("car_size")
    private String car_size;
    @SerializedName("notes")
    private String notes;

    public String getCar_size() {
        return car_size;
    }

    public void setCar_size(String car_size) {
        this.car_size = car_size;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getIdentity_num() {
        return identity_num;
    }

    public void setIdentity_num(String identity_num) {
        this.identity_num = identity_num;
    }

    public String getCar_num() {
        return car_num;
    }

    public void setCar_num(String car_num) {
        this.car_num = car_num;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getsubcategory_id() {
        return subcategory_id;
    }

    public void setsubcategory_id(int subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getSuccessOrders() {
        return successOrders;
    }

    public void setSuccessOrders(int successOrders) {
        this.successOrders = successOrders;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIdentity_image() {
        return identity_image;
    }

    public void setIdentity_image(String identity_image) {
        this.identity_image = identity_image;
    }

    public String getInsurance_file() {
        return Insurance_file;
    }

    public void setInsurance_file(String Insurance_file) {
        this.Insurance_file = Insurance_file;
    }

    public List<Assistants> getAssistants() {
        return assistants;
    }

    public void setAssistants(List<Assistants> assistants) {
        this.assistants = assistants;
    }
}
