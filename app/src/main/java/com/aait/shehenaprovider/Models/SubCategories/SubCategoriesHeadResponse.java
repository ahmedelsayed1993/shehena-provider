package com.aait.shehenaprovider.Models.SubCategories;

import java.util.List;

/**
 * Created by agamidev on 9/13/17.
 */

public class SubCategoriesHeadResponse {


    private List<SubCategoriesDataResponse> data;

    public List<SubCategoriesDataResponse> getData() {
        return data;
    }

    public void setData(List<SubCategoriesDataResponse> data) {
        this.data = data;
    }


}
