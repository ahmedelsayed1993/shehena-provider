package com.aait.shehenaprovider.Models.NewOrdersDetailsModel;

import com.aait.shehenaprovider.Models.AssistantsModel2;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Order_data implements Serializable{
    @SerializedName("order_num")
    private int order_num;
    @SerializedName("date")
    private String date;
    @SerializedName("payment_type")
    private String payment_type;
    @SerializedName("status")
    private int status;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("price")
    private String price;
    @SerializedName("service_name")
    private String service_name;

    @SerializedName("time")
    private String time;
    @SerializedName("user_phone")
    private String user_phone;

    @SerializedName("assistant")
    private List<AssistantsModel2> assistant;

    public int getOrder_num() {
        return order_num;
    }

    public void setOrder_num(int order_num) {
        this.order_num = order_num;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public List<AssistantsModel2> getAssistant() {
        return assistant;
    }

    public void setAssistant(List<AssistantsModel2> assistant) {
        this.assistant = assistant;
    }
}
