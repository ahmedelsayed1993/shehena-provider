package com.aait.shehenaprovider.Models.AboutModel;

import com.google.gson.annotations.SerializedName;

public class AllSocial {
    @SerializedName("social_id")
    private int social_id;
    @SerializedName("social_name")
    private String social_name;
    @SerializedName("social_url")
    private String social_url;
    @SerializedName("social_icon")
    private String social_icon;
    @SerializedName("social_image")
    private String social_image;

    public int getSocial_id() {
        return social_id;
    }

    public void setSocial_id(int social_id) {
        this.social_id = social_id;
    }

    public String getSocial_name() {
        return social_name;
    }

    public void setSocial_name(String social_name) {
        this.social_name = social_name;
    }

    public String getSocial_url() {
        return social_url;
    }

    public void setSocial_url(String social_url) {
        this.social_url = social_url;
    }

    public String getSocial_icon() {
        return social_icon;
    }

    public void setSocial_icon(String social_icon) {
        this.social_icon = social_icon;
    }

    public String getSocial_image() {
        return social_image;
    }

    public void setSocial_image(String social_image) {
        this.social_image = social_image;
    }
}
