package com.aait.shehenaprovider.Models.AcceptOrderModel;

import com.google.gson.annotations.SerializedName;

public class AcceptProcess_array {
    @SerializedName("number")
    private String number;
    @SerializedName("status")
    private String status;
    @SerializedName("date")
    private int date;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}
