package com.aait.shehenaprovider.Models.UpdateProcessArrayModel;

import com.aait.shehenaprovider.Models.Process_array;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 9/21/2017.
 */

public class updateProcessArrModel {

    @SerializedName("staus")
    private String staus;
    @SerializedName("message")
    private String message;
    @SerializedName("level")
    private int level;
    @SerializedName("array")
    private List<Process_array> array;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Process_array> getArray() {
        return array;
    }

    public void setArray(List<Process_array> array) {
        this.array = array;
    }
}
