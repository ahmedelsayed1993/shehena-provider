package com.aait.shehenaprovider.Models.SubCategories;

import java.io.Serializable;

/**
 * Created by agamidev on 9/13/17.
 */

public class SubCategoriesDataResponse implements Serializable {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
