package com.aait.shehenaprovider.Models;

import com.google.gson.annotations.SerializedName;

public class AssistantModel {
    @SerializedName("assistant_name")
    private String assistant_name;
    @SerializedName("assistant_job")
    private String assistant_job;

    public String getAssistant_name() {
        return assistant_name;
    }

    public void setAssistant_name(String assistant_name) {
        this.assistant_name = assistant_name;
    }

    public String getAssistant_job() {
        return assistant_job;
    }

    public void setAssistant_job(String assistant_job) {
        this.assistant_job = assistant_job;
    }
}


