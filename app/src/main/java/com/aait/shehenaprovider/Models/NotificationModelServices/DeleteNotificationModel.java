package com.aait.shehenaprovider.Models.NotificationModelServices;

/**
 * Created by Aya on 9/21/2017.
 */

public class DeleteNotificationModel {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
