package com.aait.shehenaprovider.Models.NewOrdersDetailsModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NewOrderDetailsData implements Serializable{
    @SerializedName("user_data")
    private List<User_data> user_data;
    @SerializedName("order_data")
    private List<Order_data> order_data;
    @SerializedName("order_processing")
    private int order_processing;

    public List<User_data> getUser_data() {
        return user_data;
    }

    public void setUser_data(List<User_data> user_data) {
        this.user_data = user_data;
    }

    public List<Order_data> getOrder_data() {
        return order_data;
    }

    public void setOrder_data(List<Order_data> order_data) {
        this.order_data = order_data;
    }

    public int getOrder_processing() {
        return order_processing;
    }

    public void setOrder_processing(int order_processing) {
        this.order_processing = order_processing;
    }
}
