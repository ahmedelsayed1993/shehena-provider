package com.aait.shehenaprovider.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 2/13/2018.
 */

public class deleteAss {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
