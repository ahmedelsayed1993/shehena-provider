package com.aait.shehenaprovider.Models.AboutModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("logo")
    private String logo;
    @SerializedName("about")
    private String about;
    @SerializedName("allSocial")
    private List<AllSocial> allSocial;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<AllSocial> getAllSocial() {
        return allSocial;
    }

    public void setAllSocial(List<AllSocial> allSocial) {
        this.allSocial = allSocial;
    }
}
