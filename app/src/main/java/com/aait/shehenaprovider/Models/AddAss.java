package com.aait.shehenaprovider.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 2/13/2018.
 */

public class AddAss {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<AddAssData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AddAssData> getData() {
        return data;
    }

    public void setData(List<AddAssData> data) {
        this.data = data;
    }
}
