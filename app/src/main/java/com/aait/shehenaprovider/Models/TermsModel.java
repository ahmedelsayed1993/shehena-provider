package com.aait.shehenaprovider.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 9/10/2017.
 */

public class TermsModel {

    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private String data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
