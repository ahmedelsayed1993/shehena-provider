package com.aait.shehenaprovider.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Process_array implements Serializable {
    @SerializedName("number")
    private String number;
    @SerializedName("status")
    private int status;
    @SerializedName("date")
    private String date;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
