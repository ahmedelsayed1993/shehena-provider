package com.aait.shehenaprovider.Models.BankModel;

import com.google.gson.annotations.SerializedName;

public class BanksData {
    @SerializedName("id")
    private int id;
    @SerializedName("bank_name_en")
    private String bank_name_en;
    @SerializedName("bank_number")
    private String bank_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name_en() {
        return bank_name_en;
    }

    public void setBank_name_en(String bank_name_en) {
        this.bank_name_en = bank_name_en;
    }

    public String getBank_number() {
        return bank_number;
    }

    public void setBank_number(String bank_number) {
        this.bank_number = bank_number;
    }
}
