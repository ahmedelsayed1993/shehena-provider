package com.aait.shehenaprovider.Models.NewPassword;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aya on 9/18/2017.
 */

public class ChangePasswordModel implements Serializable {


    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
