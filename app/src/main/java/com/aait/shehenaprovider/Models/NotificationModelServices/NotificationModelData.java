package com.aait.shehenaprovider.Models.NotificationModelServices;

import com.google.gson.annotations.SerializedName;

public class NotificationModelData {
    @SerializedName("id")
    private String id;
    @SerializedName("message")
    private String message;
    @SerializedName("order_id")
    private int order_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }
}
