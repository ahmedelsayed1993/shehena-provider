package com.aait.shehenaprovider.Models.FinishModel;

import com.google.gson.annotations.SerializedName;

public class FinishProcess_array {
    @SerializedName("number")
    private String number;
    @SerializedName("status")
    private String status;
    @SerializedName("date")
    private String date;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
