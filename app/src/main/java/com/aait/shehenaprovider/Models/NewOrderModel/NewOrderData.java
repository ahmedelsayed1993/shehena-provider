package com.aait.shehenaprovider.Models.NewOrderModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewOrderData implements Serializable {
    @SerializedName("order_id")
    private int order_id;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("order_num")
    private String order_num;
    @SerializedName("payment_type")
    private String payment_type;
    @SerializedName("price")
    private String price;
    @SerializedName("date")
    private String date;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("order_processing")
    private String order_processing;
    @SerializedName("category")
    private String category;
    @SerializedName("service_name")
    private String service_name;
    @SerializedName("time")
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getOrder_processing() {
        return order_processing;
    }

    public void setOrder_processing(String order_processing) {
        this.order_processing = order_processing;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

}
