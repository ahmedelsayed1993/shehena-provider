package com.aait.shehenaprovider.Models.LoginModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Aya on 8/29/2017.
 */

public class loginModel implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private LoginData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }


}
