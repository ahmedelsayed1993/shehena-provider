package com.aait.shehenaprovider.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.FCM.Config;
import com.aait.shehenaprovider.FCM.NotificationUtils;
import com.aait.shehenaprovider.Models.LoginModel.LoginData;
import com.aait.shehenaprovider.Models.LoginModel.loginModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by mahmoud on 6/19/2017.
 */

public class LoginActivity extends ParentActivity implements View.OnClickListener {
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.tv_forget_password)
    TextView tv_forget_password;
    @BindView(R.id.tv_no_account)
    TextView tv_no_account;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_password)
    EditText ed_password;
    String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {

        btn_login.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);
        tv_no_account.setOnClickListener(this);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                }
            }
        };
        displayFirebaseRegId();
    }
    private void displayFirebaseRegId() {
        SharedPreferences pref = this.getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Util.onPrintLog("Firebase reg id: " + regId);
    }
    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == btn_login) {
            if (validationLogin()) {
                getLogin();

            }
        } else if (v == tv_forget_password) {
            Intent intent=new Intent(LoginActivity.this,ForgetPassActivity.class);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (v == tv_no_account) {
            Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private boolean validationLogin() {
        if (!validatePhone()) {
            return false;
        }
        if (!validatePassword()) {
            return false;
        }
        if (regId == null) {
            Util.makeToast(this,getString(R.string.error_network));
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.enter_phone));
            Util.requestFocus(ed_phone, getWindow());
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (ed_password.getText().toString().trim().isEmpty()) {
            ed_password.setError(getString(R.string.enter_password));
            Util.requestFocus(ed_password, getWindow());
            return false;
        }
        return true;
    }
    public void getLogin() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
//        Log.i("FCMMMM", regId);
        RetroWeb.getClient().create(ServiceApi.class).LOGIN(ed_phone.getText().toString(),ed_password.getText().toString(),regId).enqueue(new Callback<loginModel>() {
            @Override
            public void onResponse(Call<loginModel> call, Response<loginModel> response) {
                Util.onPrintLog(response.body());
                if (response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        CustomeProgressDialog.onFinish();

                        if(response.body().getData().getAccount_type().equals("provider")){
                            LoginData loginData = response.body().getData();
                            globalPreferences.storeID(loginData.getId());
                            globalPreferences.storeNAME(loginData.getName());
                            globalPreferences.storeEMAIL(loginData.getEmail());
                            globalPreferences.storePHONE(loginData.getPhone());
                            globalPreferences.storeLAT(loginData.getLat());
                            globalPreferences.storeLNG(loginData.getLng());
                            globalPreferences.storeADDRESS(loginData.getAddress());
                            globalPreferences.storeAVATAR(loginData.getAvatar());
                            globalPreferences.storeLoginStatus(true);

                            Log.e("Data", loginData.getId());
//                    toaster.makeToast(response.body().getMessage());
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
//                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                            finish();
                        }else{
                            toaster.makeToast(getString(R.string.this_is_not_provider_account));
                        }


                    } else {
                        Log.i("statuuuusss", response.body().getMessage());
                        Log.i("statuuuusss", response.body().getStatus());
                        toaster.makeToast(response.body().getMessage());
                        CustomeProgressDialog.onFinish();

                    }
                }else{
                    CustomeProgressDialog.onFinish();
                    Log.i("NullData","Null Data Response In Login");
                }
            }

            @Override
            public void onFailure(Call<loginModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();
            }

        });
    }
}
