package com.aait.shehenaprovider.Activities;

import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.R;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by mahmoud on 6/19/2017.
 */

public class CodeActivity extends ParentActivity  {
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.ed_code)
    EditText ed_code;
    String code="";
    String userID="";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_code;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {
        code=getIntent().getStringExtra("code");
        toaster.makeToast(code);
        userID=getIntent().getStringExtra("userID");
        Log.e("code",code);
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }




    @OnClick(R.id.btn_send)
    void Send() {
        if (ed_code.getText().toString().equals(code)){
            Intent intent=new Intent(CodeActivity.this,RestoreNewPassActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("id", userID);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
            startActivity(intent);
            finish();
        }
        else {
            toaster.makeToast(getString(R.string.confirm_code_is_false));
        }

    }



}
