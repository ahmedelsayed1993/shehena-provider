package com.aait.shehenaprovider.Activities;

import android.widget.RelativeLayout;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.R;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class CongreatulationActivity extends ParentActivity {

    @BindView(R.id.cv_logo)
    CircleImageView cv_logo;
    @BindView(R.id.rl_cong)
    RelativeLayout rl_cong;


    @OnClick(R.id.rl_cong)
    void Cong()
    {

        finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_congratulation;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }
}
