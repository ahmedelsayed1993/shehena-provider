package com.aait.shehenaprovider.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity2 extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    Geocoder geocoder;
    static String address="",la,lo;
    Double lat,lon;
    TextView textViewAddress;
    GlobalPreferences globalPreferences;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        globalPreferences = new GlobalPreferences(this);
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(globalPreferences.getLang())); // API 17+ only.
        res.updateConfiguration(conf, dm);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps2);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
        } else {
            showGPSDisabledAlertToUser();
        }
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        textViewAddress =(TextView) findViewById(R.id.textView);
    }
    @Override protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }
    @Override protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            Location userCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (userCurrentLocation != null) {
                MarkerOptions currentUserLocation = new MarkerOptions();
                LatLng currentUserLatLang = new LatLng(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude());
                currentUserLocation.position(currentUserLatLang).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)).title(getResources().getString(R.string.location));
                mMap.addMarker(currentUserLocation);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentUserLatLang, 16));
                lat = userCurrentLocation.getLatitude();
                lon = userCurrentLocation.getLongitude();
                List<Address> addresses2;
                geocoder = new Geocoder(MapsActivity2.this, Locale.getDefault());
                try {
                    addresses2 = geocoder.getFromLocation(userCurrentLocation.getLatitude(), userCurrentLocation.getLongitude(), 1);
                    if (addresses2.isEmpty()){
                        Toast.makeText(MapsActivity2.this, getResources().getString(R.string.select_real_location), Toast.LENGTH_SHORT).show();
                    }
                    else{address = addresses2.get(0).getAddressLine(0);
                        textViewAddress.setText(address);}
                } catch (IOException e) {}
                mMap.clear();
                mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)).position(currentUserLatLang).title(getResources().getString(R.string.location)));
                try{
                    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            lat = latLng.latitude;
                            lon = latLng.longitude;
                            List<Address> addresses;
                            geocoder = new Geocoder(MapsActivity2.this, Locale.getDefault());
                            try {
                                addresses = geocoder.getFromLocation(lat, lon, 1);
                                if (addresses.isEmpty()){
                                    Toast.makeText(MapsActivity2.this, getResources().getString(R.string.select_real_location), Toast.LENGTH_SHORT).show();
                                }
                                else{address = addresses.get(0).getAddressLine(0);
                                    textViewAddress.setText(address);}

                            } catch (IOException e) {}
                            mMap.clear();
                            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)).position(latLng).title(getResources().getString(R.string.location)));
                        }
                    });
                }catch (Exception e){
                }
            }
        }
    }
    @Override
    public void onConnectionSuspended(int i) {}
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);



        //select address when  click on map wit out open my location
        try{
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    lat = latLng.latitude;
                    lon = latLng.longitude;
                    List<Address> addresses;
                    geocoder = new Geocoder(MapsActivity2.this, Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(lat, lon, 1);
                        if (addresses.isEmpty()){
                            Toast.makeText(MapsActivity2.this, getResources().getString(R.string.select_real_location), Toast.LENGTH_SHORT).show();
                        }
                        else{address = addresses.get(0).getAddressLine(0);
                            textViewAddress.setText(address);}

                    } catch (IOException e) {}
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)).position(latLng).title(getResources().getString(R.string.location)));
                }
            });
        }catch (Exception e){
        }

    }
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    public void btnSelect(View view) {
        if (!textViewAddress.getText().toString().isEmpty()&&!String.valueOf(lat).isEmpty()&&!String.valueOf(lon).isEmpty()){
            address=textViewAddress.getText().toString();
            la=String.valueOf(lat);
            lo=String.valueOf(lon);
            this.finish();}
        else {
            Toast.makeText(this, getResources().getString(R.string.click_on_map_to_select_your_location), Toast.LENGTH_SHORT).show();
        }
    }
}
