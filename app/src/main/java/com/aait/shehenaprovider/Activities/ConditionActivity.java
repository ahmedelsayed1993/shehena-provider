package com.aait.shehenaprovider.Activities;

import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.Models.TermsModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/19/2017.
 */

public class ConditionActivity extends ParentActivity {
    @BindView(R.id.tv_terms)
    TextView tv_terms;
    String terms="";




    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {
        getCondition();

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    public void getCondition() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCondition(globalPreferences.getLang()).enqueue(new Callback<TermsModel>() {
            @Override
            public void onResponse(Call<TermsModel> call, Response<TermsModel> response) {
                Util.onPrintLog(response.body());
                if (response.body() == null){
                    toaster.makeToast(getString(R.string.SomeThingWentWrong));
                }else {
                    Log.i("Terms","termsOpened!");

                    tv_terms.setText(Html.fromHtml(response.body().getData()));
                }
                CustomeProgressDialog.onFinish();
            }

            @Override
            public void onFailure(Call<TermsModel> call, Throwable t) {

            }

        });
    }


}
