package com.aait.shehenaprovider.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.Models.ForgetPasswordModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by mahmoud on 6/19/2017.
 */

public class ForgetPassActivity extends ParentActivity implements View.OnClickListener {
    @BindView(R.id.btn_restore)
    Button btn_restore;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    String code="";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_restore_password;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }


    @Override
    public void onClick(View v) {

    }

    @OnClick(R.id.btn_restore)
    void Restore() {
        if (validationRestore()) {
            getForgetPassword();
        }
    }

    private boolean validationRestore() {
        if (!validatePhone()) {
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.enter_phone));
            Util.requestFocus(ed_phone, getWindow());
            return false;
        }
        return true;
    }
    public void getForgetPassword() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getForgetPassword(ed_phone.getText().toString(),globalPreferences.getLang()).enqueue(new Callback<ForgetPasswordModel>() {
            @Override
            public void onResponse(Call<ForgetPasswordModel> call, Response<ForgetPasswordModel> response) {
                Util.onPrintLog(response.body());
                CustomeProgressDialog.onFinish();

                if (response.body()!=null) {
                    if (response.body().getValue().equals("1")) {
                        if (response.body().getAccount_type().equals("provider")){
                            code = response.body().getCode() + "";
                            // toaster.makeToast(response.body().getMessage());
                            Intent intent = new Intent(ForgetPassActivity.this, CodeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("code", code);
                            toaster.makeToast(response.body().getCode()+"");
                            intent.putExtra("userID", response.body().getUser_id());
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                            startActivity(intent);
                            finish();
                        }
                        else{toaster.makeToast(getString(R.string.this_is_not_provider_account));}

                    } else { toaster.makeToast(getString(R.string.enter_phone)); }
                }
                else
                { toaster.makeToast(getString(R.string.enter_phone)); }
            }


            @Override
            public void onFailure(Call<ForgetPasswordModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();

            }

        });
    }

}
