package com.aait.shehenaprovider.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Constant;
import com.aait.shehenaprovider.Utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mahmoud on 6/19/2017.
 */

public  class RegisterActivity extends ParentActivity  {
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.ed_city)
    EditText ed_city;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_paeeword)
    EditText ed_paeeword;
    @BindView(R.id.cv_photo)
    CircleImageView cv_photo;
    public String filePath, profileImageFilePath;
    String ImageBase64="";
    String latiud, langitud,city;
    Intent intent;



    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {

    }
    @Override
    protected void onResume() {
        if(!MapsActivity2.address.isEmpty()){ed_city.setText(MapsActivity2.address);}
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        MapsActivity2.address="";
        MapsActivity2.address="";
        MapsActivity2.address="";
        super.onDestroy();
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @OnClick(R.id.btn_next)
    void Next() {
        if (validationRegister()) {
            Intent intent=new Intent(RegisterActivity.this,RegisterCarActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("name",ed_name.getText().toString());
            intent.putExtra("email",ed_email.getText().toString());
            intent.putExtra("city",ed_city.getText().toString());
            intent.putExtra("phone",ed_phone.getText().toString());
            intent.putExtra("password",ed_paeeword.getText().toString());
            intent.putExtra("lat",MapsActivity2.la);
            intent.putExtra("lng",MapsActivity2.lo);
            intent.putExtra("pic",ImageBase64);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
            startActivity(intent);
            //finish();

        }
    }


    private boolean validationRegister() {
        if (!validateName()) {
            return false;
        }
        if (!validateEmail()) {
            return false;
        }

        if (!validateCity()) {
            return false;
        }
        if (!LatLng()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }
        if (!validatePassword()) {
            return false;
        }
        if(!validatePhoto())
        {
            return false;
        }


        return true;
    }
    public boolean validatePhoto() {
        if (ImageBase64.equals("")) {
            toaster.makeToast(getString(R.string.enter_photo));
            return false;
        }
        return true;

    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.enter_phone));
            Util.requestFocus(ed_phone, getWindow());
            return false;
        }

            else if (ed_phone.getText().toString().length()<8)
            {
                ed_phone.setError(getString(R.string.enter_phone_eight));
                Util.requestFocus(ed_phone, getWindow());
                return false;
            }

        return true;
    }

    private boolean validatePassword() {
        if (ed_paeeword.getText().toString().trim().isEmpty()) {
            ed_paeeword.setError(getString(R.string.enter_password));
            Util.requestFocus(ed_paeeword, getWindow());
            return false;
        }
        else if (ed_paeeword.getText().toString().length()<6)
        {
            ed_paeeword.setError(getString(R.string.enter_password_six));
            Util.requestFocus(ed_paeeword, getWindow());
            return false;
        }
        return true;
    }

    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            ed_name.setError(getString(R.string.enter_name));
            Util.requestFocus(ed_name, getWindow());
            return false;
        }
        return true;
    }

    private boolean validateCity() {
        if (ed_city.getText().toString().trim().isEmpty()) {
            ed_city.setError(getString(R.string.enter_city));
            Util.requestFocus(ed_city, getWindow());
            return false;
        }
        return true;
    }

    private boolean validateEmail() {
        if (ed_email.getText().toString().trim().isEmpty()) {
            ed_email.setError(getString(R.string.enter_email));
            Util.requestFocus(ed_email, getWindow());
            return false;
        }
       else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(ed_email.getText().toString()).matches()) {
                ed_email.setError(getString(R.string.email_formate));
                Util.requestFocus(ed_email, getWindow());
                return false;
            }
        }
        return true;
    }
    @OnClick(R.id.ed_city)
    void City() {
        Intent intent = new Intent(this, MapsActivity2.class);
        startActivityForResult(intent, Constant.RequestCode.GETLOCATION);

    }
    @OnClick(R.id.cv_photo)
    void Add_picture() {
        showPictureDialog();
    }

    // Choose Picture events
    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)
        };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);

    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            handleChoosePhotoFromGallery(data, cv_photo);

        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            handleTakePhotoFromCamera(data);
        }
        else if (requestCode == Constant.RequestCode.GETLOCATION) {
            // Log.d("jj", mAdresse + mLat + mLang);

            if (data != null) {
                 city= data.getStringExtra(Constant.LOCATION);
                langitud = data.getStringExtra(Constant.LNG);
                latiud = data.getStringExtra(Constant.LAT);
                ed_city.setText(city);
            }

        }
    }
    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void setPic(ImageView mImageView, String currentPath) {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        ImageBase64 = Util.getEncoded64ImageStringFromBitmap(bitmap);
        //Log.e(TAG, "base64: " + ImageBase64);
    }
    private void handleTakePhotoFromCamera(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");

        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri = getImageUri(RegisterActivity.this, imageBitmap);

        // CALL THIS METHOD TO GET THE ACTUAL PATH
        File finalFile = new File(getRealPathFromURI(tempUri));

        // Set Image to Picture
        setPic(cv_photo, finalFile.getAbsolutePath());
        ImageBase64 = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
       // Log.e(TAG, "base64: " + ImageBase64);
       // Log.e(TAG, "uri:" + tempUri);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    private boolean LatLng() {
        if (MapsActivity2.la == null || MapsActivity2.lo == null) {
            //Log.e(TAG, "LatLng = null");
            return false;
        } else {
            return true;
        }
    }
   /* @Override
    public void onFinishDetectAddressDialog(String inputText, String lat, String lang) {
        ed_city.setText(inputText);
       // Log.e(TAG, "Lat" + lat + "Lng" + lang);
        latiud = lat;
        langitud = lang;
    }*/
}
