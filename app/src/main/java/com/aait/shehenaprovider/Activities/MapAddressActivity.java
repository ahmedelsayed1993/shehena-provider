package com.aait.shehenaprovider.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.GPS.GPSTracker;
import com.aait.shehenaprovider.GPS.GPSTrakerListner;
import com.aait.shehenaprovider.Models.LocationResponse.LocationResponse;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Constant;
import com.aait.shehenaprovider.Utils.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/7/2017.
 */

public class MapAddressActivity extends ParentActivity implements OnMapReadyCallback, GPSTrakerListner, GoogleMap.OnMapClickListener {

    public static String TAG = MapAddressActivity.class.getSimpleName();
    @BindView(R.id.tv_ditected_succ)
    TextView tv_ditected_succ;
    @BindView(R.id.btn_finish_location)
    Button btn_finish_location;
    @BindView(R.id.map)
    MapView mapView;
    GoogleMap googleMap;
    Marker myMarker;
    GPSTracker gps;
    public String mLang, mLat;
    private ProgressDialog progressDialogLocation;
    boolean startTraker = false;
    String mResult;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.iv_detect_location)
    ImageView iv_detect_location;
    Geocoder geocoder;
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_locate_address;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @OnClick(R.id.iv_detect_location)
    void detect_location() {
        getLocaionWithPermission();
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @OnClick(R.id.iv_back)
    void back() {
        onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocaionWithPermission();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("LatLng", latLng.toString());
        mLang = Double.toString(latLng.latitude);
        mLat = Double.toString(latLng.longitude);
        if (myMarker != null) {
            myMarker.remove();
            putMapMarker(latLng.latitude, latLng.longitude);
        } else {
            putMapMarker(latLng.latitude, latLng.longitude);
        }
        Log.e(TAG, "onMapClicl " + "Lat:" + mLat + " Lang:" + mLang);
    }

    @OnClick(R.id.btn_finish_location)
    void Ditected_succ() {
        Log.e("Location", "Lat:" + mLat + " Lng:" + mLang+mResult);
        if (mLang!=null&&mLat!=null&&mResult!=null) {
            Intent intentData = new Intent();
            intentData.putExtra(Constant.LOCATION, mResult);
            intentData.putExtra(Constant.LAT, mLat);
            intentData.putExtra(Constant.LNG, mLang);
            setResult(RESULT_OK, intentData);
            finish();
        }
    }
    public void getLocationInfo(final String lat, final String lng, final String lang) {
        mLang =lng;
        mLat = lat;
        RetroWeb.getLocationClint().create(ServiceApi.class).getLocation(lat + "," + lng, lang).enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {

                LocationResponse mlocation = response.body();
                Util.onPrintLog(mlocation);
                //TODO Errorr
                if (mlocation.getResults().size() != 0) {
                    if (mlocation.getResults().get(0).getFormattedAddress() != null) {
                        mResult = mlocation.getResults().get(0).getFormattedAddress();
                        // toaster.makeToast(mResult);
                        tv_location.setText(mResult);
                    }
                }else {
                    toaster.makeToast("empty");
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Util.handleException(MapAddressActivity.this, t);
                Util.onPrintLog(t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public static void starMapActivity(AppCompatActivity context) {

    }

    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
       // getLocationInfo(""+lat,""+log,"ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.map_marker));
        marker.title("موقعي");
        myMarker = googleMap.addMarker(marker);
    }

    @Override
    public void onTrakerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTraker) {
            if (lat != 0.0 && log != 0.0) {
                progressDialogLocation.dismiss();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTraker() {
        startTraker = true;
        progressDialogLocation = ProgressDialog.show(this, "الرجاء الإنتظار ...",
                "جاري تحديد الموقع الخاص بك ....", true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Code", "requestCode: " + requestCode);
        switch (requestCode) {
            case 300: {
                getCurrentLocation();
                if (resultCode == RESULT_OK) {
                    Log.e("Code", "request GPS Enabled True");
                    getCurrentLocation();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 100: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    Log.e("Permission", "All permission are granted");
                    Log.e("Permission", PackageManager.PERMISSION_GRANTED + "");
                    getCurrentLocation();
                    for (int i = 0; i < grantResults.length; i++) {
                        Log.e("Permission", grantResults[0] + "");
                    }
                } else {
                    Log.e("Permission", "permission arn't granted");
                    // do action here if the permission isn't granted
                }
                return;
            }
        }
    }


    public void getLocaionWithPermission() {
        gps = new GPSTracker(this, this);
        if (canMakeSmores()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                List<String> permissionsNeeded = new ArrayList<String>();
                final List<String> permissionsList = new ArrayList<String>();
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
                    permissionsNeeded.add("GPS");
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
                    permissionsNeeded.add("Location");

                if (permissionsList.size() > 0) {
                    if (permissionsNeeded.size() > 0) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                    100);
                        }
                    }
                }
                Log.e("GPS", "1");
            } else {
                Log.e("GPS", "2");
                getCurrentLocation();
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    // run time permission
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }



    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("إعدادات تحديد الموقع");
        // Setting Dialog Message
        alertDialog.setMessage("هل تريد السماح بإمكانية استخدام خاصية تحديد الموقع ؟");
        // On pressing Settings button
        alertDialog.setPositiveButton("إعدادات", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.RequestCode.GPSEnabling);

            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("رفض", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            showSettingsAlert();
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                geocoder = new Geocoder(MapAddressActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()){
                        Toast.makeText(MapAddressActivity.this, "لم يتم تحديد موقع", Toast.LENGTH_SHORT).show();
                    }
                    else{mResult = addresses.get(0).getAddressLine(0);
                         tv_location.setText(mResult);
                    }

                } catch (IOException e) {}
                googleMap.clear();
                putMapMarker(gps.getLatitude(), gps.getLongitude());
            }
        }
    }

}
