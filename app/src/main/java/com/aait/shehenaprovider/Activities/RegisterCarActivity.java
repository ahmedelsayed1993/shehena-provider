package com.aait.shehenaprovider.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.FCM.Config;
import com.aait.shehenaprovider.FCM.NotificationUtils;
import com.aait.shehenaprovider.Models.AssistantModel;
import com.aait.shehenaprovider.Models.Categories.CategoriesDataResponse;
import com.aait.shehenaprovider.Models.Categories.CategoriesHeadResponse;
import com.aait.shehenaprovider.Models.RegisterModel.RegisterData;
import com.aait.shehenaprovider.Models.RegisterModel.registerModel;
import com.aait.shehenaprovider.Models.SubCategories.SubCategoriesDataResponse;
import com.aait.shehenaprovider.Models.SubCategories.SubCategoriesHeadResponse;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Constant;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterCarActivity extends ParentActivity {
    public static TextView tv_title;
    @BindView(R.id.tv_condition)
    TextView tv_condition;
    @BindView(R.id.btn_register)
    Button btn_register;
    @BindView(R.id.ed_id_number)
    EditText ed_id_number;
    @BindView(R.id.ed_plate_number)
    EditText ed_plate_number;
    @BindView(R.id.ed_service_price)
    EditText ed_service_price;
    @BindView(R.id.sp_sub_service)
    Spinner sp_sub_service;
    @BindView(R.id.sp_driving_license)
    Spinner sp_driving_license;
    @BindView(R.id.ed_help)
    EditText ed_help;
    @BindView(R.id.ed_job)
    EditText ed_job;
    @BindView(R.id.tv_add)
    TextView tv_add;
    @BindView(R.id.iv_cam_file)
    ImageView iv_cam_file;
    @BindView(R.id.iv_cam_license)
    ImageView iv_cam_license;
    @BindView(R.id.rl_help)
    RelativeLayout layout;
    @BindView(R.id.ed_notes)
    EditText ed_notes;
    @BindView(R.id.ed_size)
    EditText ed_size;
    public String filePath, profileImageFilePath;
    String ImageBaseLicense="";
    String ImageBasefile="";
    String name,email,city,lat,lng,phone,password,pic;
    int numberOfTextViews = 0;
    int numberOfTextViews1 = 0;

    int count=20;
    EditText[] editTexts = new EditText[count];
    EditText[] editTexts1 = new EditText[count];

    List<AssistantModel> strings;
    List<String> text;
    List<String> text1;

    AssistantModel assistantNameModel;
    EditText editText;
    EditText editText1;

    int mA=0;
    String id_category = "";
    String id_subcategory="";
    ArrayList<String> categories= new ArrayList<>();
    String[] spinnerItemsForCategories;
    ArrayList<String> sub_categories= new ArrayList<>();
    String m="";
    String n="";
    String[] spinnerItemsForSubCategories;
    String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register_car;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {
        strings = new ArrayList<>();
        text = new ArrayList<>();
        text1 = new ArrayList<>();

        editTexts[0] = ed_help;
        editTexts1[0] = ed_job;
        tv_title = (TextView) findViewById(R.id.tv_title);
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        city = getIntent().getStringExtra("city");
        lat = getIntent().getStringExtra("lat");
        lng = getIntent().getStringExtra("lng");
        phone = getIntent().getStringExtra("phone");
        password = getIntent().getStringExtra("password");
        pic = getIntent().getStringExtra("pic");
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) ed_help.getLayoutParams();
        lp.width=200;
        RelativeLayout.LayoutParams ld = (RelativeLayout.LayoutParams) ed_job.getLayoutParams();
        ld.width=200;
//        Log.e("pass",password);
       // Log.e("Bundle Data", name + "  " + email + "  " + city + "  " + lat + "  " + lng + "  " + phone + "  " + password + "  " + pic);

       getCategories();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                }
            }
        };

        displayFirebaseRegId();

    }
    private void displayFirebaseRegId() {
        SharedPreferences pref = this.getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Util.onPrintLog("Firebase reg id: " + regId);
    }
    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @OnClick(R.id.iv_cam_license)
    void License() {
        mA=1;
        showPictureDialog();
    }
    @OnClick(R.id.iv_cam_file)
    void File() {
        mA=2;
        showPictureDialog();
    }
    @OnClick(R.id.btn_register)
    void Register() {

       if (validationRegister()) {
           getRegister();
        }
    }
    @OnClick(R.id.tv_condition)
    void Conditions() {
        Intent intent=new Intent(RegisterCarActivity.this,ConditionActivity.class);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        //finish();
    }
    void AddEditText() {
        if (!ed_help.getText().toString().equals("")&&!ed_job.getText().toString().equals("")) {
            Log.e("num", numberOfTextViews + "");

            if (numberOfTextViews < count) {
                RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams
                        (200, RelativeLayout.LayoutParams.WRAP_CONTENT);
                lparams.addRule(RelativeLayout.BELOW, editTexts[numberOfTextViews].getId());
                lparams.setMargins(250, 10, 0, 0);
                editText = new EditText(RegisterCarActivity.this);
                assistantNameModel = new AssistantModel();
                editText.setHint(getString(R.string.another_help_name));
                editText.setId(++numberOfTextViews);
                editText.setTextSize(16);
                editText.setTextColor(Color.BLACK);
                editText.setBackgroundResource(R.drawable.round_rectangle);
                editText.setGravity(Gravity.RIGHT);
                editTexts[numberOfTextViews] = editText;
                editText.setSingleLine(true);


                layout.addView(editTexts[numberOfTextViews], lparams);
                //strings.add(""+editTexts[numberOfTextViews-1].getId());
            }

            if (numberOfTextViews1 < count) {
                RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams
                        (200, RelativeLayout.LayoutParams.WRAP_CONTENT);
                lparams.addRule(RelativeLayout.BELOW, editTexts1[numberOfTextViews1].getId());
                lparams.setMargins(0, 10, 0, 0);
                editText1 = new EditText(RegisterCarActivity.this);
                assistantNameModel = new AssistantModel();
                editText1.setHint(getString(R.string.job));
                editText1.setId(++numberOfTextViews1);
                editText1.setTextSize(16);
                editText1.setTextColor(Color.BLACK);
                editText1.setBackgroundResource(R.drawable.round_rectangle);
                editText1.setGravity(Gravity.RIGHT);
                editTexts1[numberOfTextViews1] = editText1;
                editText.setSingleLine(true);

                layout.addView(editTexts1[numberOfTextViews1], lparams);
                //strings.add(""+editTexts[numberOfTextViews-1].getId());
            }







    }


    }
    @OnClick(R.id.tv_add)
    void Add() {
       if (numberOfTextViews>0) {
           if (!editText1.getText().toString().isEmpty() && !editText.getText().toString().isEmpty())
           {
               AddEditText();
           Log.e("num1", numberOfTextViews + "");
       }

        }
        if (numberOfTextViews==0) {
           AddEditText();

       }

    }


    private boolean validationRegister() {
        if (!validateId()) {
            return false;
        }
        if (!validatePlate()) {
            return false;
        }
        if (!validatePrice()) {
            return false;
        }
        if (!validateSize())
        {
            return false;
        }
        if (!validatePhotoFile()) {
            return false;
        }
        if (!validatePhotoLicense()) {
            return false;
        }
        if (regId == null) {
            Util.makeToast(this,getString(R.string.error_network));
        }

        return true;
    }

    private boolean validateId() {
        if (ed_id_number.getText().toString().trim().isEmpty()) {
            ed_id_number.setError(getString(R.string.enter_id));
            Util.requestFocus(ed_id_number, getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePlate() {
        if (ed_plate_number.getText().toString().trim().isEmpty()) {
            ed_plate_number.setError(getString(R.string.enter_plate));
            Util.requestFocus(ed_plate_number, getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePrice() {
        if (ed_service_price.getText().toString().trim().isEmpty()) {
            ed_service_price.setError(getString(R.string.enter_price));
            Util.requestFocus(ed_service_price, getWindow());
            return false;
        }
        return true;
    }
    private boolean validateSize() {
        if (ed_size.getText().toString().trim().isEmpty()) {
            ed_size.setError(getString(R.string.enter_size));
            Util.requestFocus(ed_size, getWindow());
            return false;
        }
        return true;
    }
    public boolean validatePhotoLicense() {
        if (ImageBaseLicense.equals("")) {
            toaster.makeToast(getString(R.string.enter_photo_license));
            return false;
        }
        return true;

    }
    public boolean validatePhotoFile() {
        if (ImageBasefile.equals("")) {
            toaster.makeToast(getString(R.string.enter_photo_file));
            return false;
        }
        return true;

    }


    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(RegisterCarActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(RegisterCarActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(RegisterCarActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(RegisterCarActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);
    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            if (mA==1) {
                handleChoosePhotoFromGallery(data, iv_cam_license);
            }else if (mA==2) {
                handleChoosePhotoFromGallery(data, iv_cam_file);
            }
        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            if (mA==1) {
                handleTakePhotoFromCamera(data, iv_cam_license);
            }else if (mA==2) {
                handleTakePhotoFromCamera(data, iv_cam_file);
            }
        }
    }
    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void setPic(ImageView mImageView, String currentPath) {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
       if (mA==1) {
            ImageBaseLicense = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
        else if (mA==2) {
            ImageBasefile = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
    }
    private void handleTakePhotoFromCamera(Intent data ,ImageView mImageView) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        Uri tempUri = getImageUri(RegisterCarActivity.this, imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        setPic(mImageView, finalFile.getAbsolutePath());
          if (mA==1) {
              ImageBaseLicense = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
          }
       else if (mA==2) {
            ImageBasefile = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public void SetSpinnerCategory() {
        ArrayAdapter<CharSequence> category_adapter = new ArrayAdapter<CharSequence>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForCategories
        );
        sp_driving_license.setAdapter(category_adapter);
       // sp_driving_license.setSelection(position);
    }
    public void getCategories() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).getCategory(globalPreferences.getLang()).enqueue(new Callback<CategoriesHeadResponse>() {
            @Override
            public void onResponse(Call<CategoriesHeadResponse> call, Response<CategoriesHeadResponse> response) {
                customeProgressDialog.onFinish();
                final List<CategoriesDataResponse> arrayList = response.body().getData();
                if (arrayList!=null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        categories.add(arrayList.get(i).getName());
                    }
                    spinnerItemsForCategories = new String[categories.size()];
                    spinnerItemsForCategories = categories.toArray(spinnerItemsForCategories);
                    SetSpinnerCategory();
                }
                else {customeProgressDialog.onFinish();}
                sp_driving_license.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_category=""+arrayList.get(position).getId();
                        sub_categories.clear();
                        id_subcategory="";
                        getSubCategory();
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {}
                });
            }
            @Override
            public void onFailure(Call<CategoriesHeadResponse> call, Throwable t) {
                customeProgressDialog.onFinish();}

        });
    }
    public void SetSpinnerSubCategory() {
        ArrayAdapter<CharSequence> sub_category_adapter = new ArrayAdapter<CharSequence>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForSubCategories
        );
        sp_sub_service.setAdapter(sub_category_adapter);
    }

    public void getSubCategory() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSubCategory(globalPreferences.getLang(), id_category).enqueue(new Callback<SubCategoriesHeadResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesHeadResponse> call, Response<SubCategoriesHeadResponse> response) {
                customeProgressDialog.onFinish();
                final ArrayList<SubCategoriesDataResponse> arrayList = (ArrayList<SubCategoriesDataResponse>) response.body().getData();
                Util.onPrintLog(arrayList);
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        sub_categories.add(arrayList.get(i).getName());
                    }
                    spinnerItemsForSubCategories = new String[sub_categories.size()];
                    spinnerItemsForSubCategories = sub_categories.toArray(spinnerItemsForSubCategories);
                    SetSpinnerSubCategory();
                } else {
                    customeProgressDialog.onFinish();
                }
                sp_sub_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_subcategory = arrayList.get(position).getId()+"";
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onFailure(Call<SubCategoriesHeadResponse> call, Throwable t) {
                customeProgressDialog.onFinish();
            }

        });
    }
    public void getRegister() {
        if (editTexts!=null) {
            for (int i = 0; i < editTexts.length; i++) {
                if (editTexts[i]!=null) {
                    Log.e("Strings", "" + editTexts[i].getText());
                    text.add(editTexts[i].getText().toString());
                    if (!editTexts[i].getText().toString().equals("")) {
                        assistantNameModel=new AssistantModel();
                        assistantNameModel.setAssistant_job(editTexts1[i].getText().toString());
                        assistantNameModel.setAssistant_name(editTexts[i].getText().toString());
                        Log.e("kjgkj",i+"    "+editTexts[i].getText().toString());
                        strings.add(assistantNameModel);

                    }
                    else{
                    }
                }
            }
       }
        Util.onPrintLog(strings);

        Log.e("response",name+"  "+email+"  "+password+"  "+city+"  "+lat+"  "+lng+"  "+phone+"  "+
                ed_plate_number.getText().toString()+"  "+ed_id_number.getText().toString()+"  "+ed_service_price.getText().toString()
                +"  "+id_subcategory);
       Log.e("pic",pic);
        Log.e("ImageBasefile",ImageBasefile);
        Log.e("ImageBaseLicense",ImageBaseLicense);
        Log.e("stringd",new Gson().toJson(strings));
        Log.e("regId",regId);

        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRegister(name,email,password,city,lat,lng,phone,
               ed_plate_number.getText().toString(),ed_id_number.getText().toString(),ed_service_price.getText().toString()
        ,id_subcategory,pic,ImageBasefile,ImageBaseLicense,new Gson().toJson(strings),regId,ed_size.getText().toString(),ed_notes.getText().toString()).enqueue(new Callback<registerModel>() {

            @Override
            public void onResponse(Call<registerModel> call, Response<registerModel> response) {
                CustomeProgressDialog.onFinish();
                //toaster.makeToast("register");
                Util.onPrintLog(response.body());
                if (response.body() != null) {
                    if (response.body().getStatus() != null) {
                        if (response.body().getStatus().equals("1")) {
                           // toaster.makeToast("register1");

                            CustomeProgressDialog.onFinish();
                            RegisterData registerData = response.body().getData();
                            globalPreferences.storeID(registerData.getUser_id()+"");
                            globalPreferences.storeNAME(registerData.getName());
                            globalPreferences.storeEMAIL(registerData.getEmail());
                            globalPreferences.storePHONE(registerData.getPhone());
                            globalPreferences.storeLAT(registerData.getLat());
                            globalPreferences.storeLNG(registerData.getLng());
                            globalPreferences.storeADDRESS(registerData.getAddress());
                            globalPreferences.storeCAR_NUM(registerData.getCar_num());
                            globalPreferences.storeIDENTITY_NUM(registerData.getIdentity_num());
                            globalPreferences.storePrice(registerData.getPrice());
                            globalPreferences.storeSUB_CATEGORY_ID(registerData.getSub_category_id());
                            globalPreferences.storeAVATAR(registerData.getAvatar());
                            globalPreferences.storeIDENTITY_IMAGE(registerData.getIdentity_image());
                            globalPreferences.storeINSURANCE_FILE(registerData.getInsurance_file());
                            globalPreferences.storeLoginStatus(true);

                            Intent intent = new Intent(RegisterCarActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                            finish();
                        } else {
                            toaster.makeToast(response.body().getMessage());
                            CustomeProgressDialog.onFinish();

                        }
                    } else {
                        toaster.makeToast(response.body().getMsg());
                    }
                }else {
                    Log.i("NullData", "Null Response Body In Register Car");
                }
            }

            @Override
            public void onFailure(Call<registerModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();
                Log.i("OnFailureCalled", "OnFailure In Register Car");
                Util.onPrintLog(t.getMessage());
            }

        });
    }


}
