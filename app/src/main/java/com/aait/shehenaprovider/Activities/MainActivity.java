package com.aait.shehenaprovider.Activities;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.Fragment.AboutFragment;
import com.aait.shehenaprovider.Fragment.AccountsFragment;
import com.aait.shehenaprovider.Fragment.CancelDoneFragment;
import com.aait.shehenaprovider.Fragment.CancleRequestFragment;
import com.aait.shehenaprovider.Fragment.CurrentOrdersFragment;
import com.aait.shehenaprovider.Fragment.FinishedOrdersFragment;
import com.aait.shehenaprovider.Fragment.FragmentOrderNumberFragment;
import com.aait.shehenaprovider.Fragment.NavigationFragment;
import com.aait.shehenaprovider.Fragment.NotificationFragment;
import com.aait.shehenaprovider.Fragment.OrderDetailsFragment;
import com.aait.shehenaprovider.Fragment.ProfileFragment;
import com.aait.shehenaprovider.Fragment.SettingFragment;
import com.aait.shehenaprovider.Models.NotificationModelServices.NotificationModel;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahmoud on 6/19/2017.
 */

public class MainActivity extends ParentActivity implements View.OnClickListener {

    @BindView(R.id.navigate)
    ImageView navigate;
    @BindView(R.id.iv_notification)
    ImageView iv_notification;
    public static TextView tv_title;
    public static FragmentManager fragmentManager;
    public static AppCompatActivity activity;
    public static DrawerLayout drawerLayout;
    NavigationFragment navigationFragment;
    public static TextView notification_count;
    int noti;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
        notification_count = (TextView) findViewById(R.id.notification_count);


        Util.onPrintLog(globalPreferences.getID());
        globalPreferences.storeLoginStatus(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        fragmentManager = getSupportFragmentManager();
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_notification.setOnClickListener(this);

        activity = this;
        getNotification();

        // fragment manager
        if (getIntent() != null) {
            Log.e("you are  ", "her 1");
            if (getIntent().getBooleanExtra("state", false)) {
                fragmentManager.beginTransaction().addToBackStack(NotificationFragment.class.getName()).replace(R.id.content, new NotificationFragment()).commit();
            } else {
                fragmentManager.beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();
            }
        }else {
            fragmentManager.beginTransaction().addToBackStack(CurrentOrdersFragment.class.getName()).replace(R.id.content, new CurrentOrdersFragment()).commit();
        }
        navigationFragment = new NavigationFragment();
        fragmentManager.beginTransaction().replace(R.id.nav_view, navigationFragment).commit();


    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.navigate)
    void OnNavigation() {
        if (globalPreferences.getLang().equals("en")) {
            if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        } else {
            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == iv_notification) {
            getSupportFragmentManager().beginTransaction().addToBackStack(com.aait.shehenaprovider.Fragment.NotificationFragment.class.getName()).replace(R.id.content, new NotificationFragment()).commit();
        }
    }


    @Override
    public void onBackPressed() {
        Fragment frg = getSupportFragmentManager().findFragmentById(R.id.content);
        String frg_name = frg.getClass().getName();
        if (drawerLayout.isDrawerOpen(Gravity.LEFT) || drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            if (globalPreferences.getLang().equals("en"))
                drawerLayout.closeDrawer(Gravity.LEFT);
            else {
                if (globalPreferences.getLang().equals("ar"))
                    drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        }
        if (frg.getClass().getName().equals(new CurrentOrdersFragment().getClass().getName())) {
            finish();
        } else if (frg_name.equals(new CancleRequestFragment().getClass().getName())) {
            fragmentManager.popBackStack(FragmentOrderNumberFragment.class.getName(), 0);

        } else if (frg_name.equals(new ProfileFragment().getClass().getName()) ||
                frg_name.equals(new NotificationFragment().getClass().getName()) ||
/*
                frg_name.equals(new NewOrdersFragment().getClass().getName()) ||
*/
                frg_name.equals(new FinishedOrdersFragment().getClass().getName()) ||
                frg_name.equals(new AccountsFragment().getClass().getName()) ||
                frg_name.equals(new SettingFragment().getClass().getName()) ||
                frg_name.equals(new AboutFragment().getClass().getName())
                || frg_name.equals(new FragmentOrderNumberFragment().getClass().getName())
                || frg_name.equals(new CongreatulationActivity().getClass().getName())
                || frg_name.equals(new OrderDetailsFragment().getClass().getName())
                || frg_name.equals(new CancelDoneFragment().getClass().getName())
/*
                ||frg_name.equals(new Map().getClass().getName())
*/

                ) {
            fragmentManager.popBackStack(CurrentOrdersFragment.class.getName(), 0);
        } else
            super.onBackPressed();
    }
    public void getNotification(){
        RetroWeb.getClient().create(ServiceApi.class).getNotification(globalPreferences.getID(),globalPreferences.getLang()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        if (response.body().getData().size() != 0) {
                            noti=response.body().getData().size();
                            notification_count.setText(noti+"");
                        } else {
                            notification_count.setText("0");


                        }
                    }else {
                    }
                }else{

                }

            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                toaster.makeToast("error notiii");

            }
        });

    }


}


