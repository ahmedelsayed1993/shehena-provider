package com.aait.shehenaprovider.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.Models.NewPassword.ChangePasswordModel;
import com.aait.shehenaprovider.Models.NewPassword.Data;
import com.aait.shehenaprovider.Network.RetroWeb;
import com.aait.shehenaprovider.Network.ServiceApi;
import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.Util;
import com.aait.shehenaprovider.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by mahmoud on 6/19/2017.
 */

public class RestoreNewPassActivity extends ParentActivity implements View.OnClickListener {
    @BindView(R.id.btn_send)
    Button btn_send;

    @BindView(R.id.ed_new_pass)
    EditText ed_new_pass;
    String id;
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_restore_new_password;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init() {

        id=getIntent().getStringExtra("id");
        Log.e("id",id);

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }


    @Override
    public void onClick(View v) {

    }

    @OnClick(R.id.btn_send)
    void Send() {
        if (validationRestore()) {
           getNewPassword();
        }
    }

    private boolean validationRestore() {

        if (!validatepass()) {
            return false;
        }
        return true;
    }

    private boolean validatepass() {
        if (ed_new_pass.getText().toString().trim().isEmpty()) {
            ed_new_pass.setError(getString(R.string.enter_new_password));
            Util.requestFocus(ed_new_pass, getWindow());
            return false;
        }
        else if (ed_new_pass.getText().toString().length()<6)
        {
            ed_new_pass.setError(getString(R.string.enter_password_six));
            Util.requestFocus(ed_new_pass, getWindow());
            return false;
        }
        return true;
    }

    public void getNewPassword() {
        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNewPassword(id,
                ed_new_pass.getText().toString()).enqueue(new Callback<ChangePasswordModel>() {
            @Override
            public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                Util.onPrintLog(response.body());
                CustomeProgressDialog.onFinish();

                if (response.body().getStatus().equals("1")) {
                     Data data = response.body().getData();
                    globalPreferences.storeID(data.getUser_id()+"");
                    globalPreferences.storeNAME(data.getName());
                    globalPreferences.storeEMAIL(data.getEmail());
                    globalPreferences.storePHONE(data.getPhone());
                    globalPreferences.storeLAT(data.getLat());
                    globalPreferences.storeLNG(data.getLng());
                    globalPreferences.storeADDRESS(data.getAddress());
                    globalPreferences.storeAVATAR(data.getAvatar());
                    globalPreferences.storeLoginStatus(true);
                    Intent intent = new Intent(RestoreNewPassActivity.this, MainActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                    finish();                }
                else
                    toaster.makeToast(response.body().getMessage());

            }

            @Override
            public void onFailure(Call<ChangePasswordModel> call, Throwable t) {

            }

        });
    }


}
