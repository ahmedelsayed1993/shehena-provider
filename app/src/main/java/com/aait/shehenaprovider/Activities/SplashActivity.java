package com.aait.shehenaprovider.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aait.shehenaprovider.Base.ParentActivity;
import com.aait.shehenaprovider.R;

import java.util.Locale;

import butterknife.BindView;

/*
created by mahmoud 10/4/2017
 */
public class SplashActivity extends ParentActivity {

    @BindView(R.id.cv_logo)
    ImageView cv_logo;
    @BindView(R.id.activity_splash)
    RelativeLayout activity_splash;

    Runnable run;
    Handler handler = new Handler();
    Animation translate;
    Animation fade;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void init() {
        setLocale(globalPreferences.getLang());


        fade = AnimationUtils.loadAnimation(this, R.anim.alpha);
        activity_splash.clearAnimation();
        activity_splash.startAnimation(fade);

        translate = AnimationUtils.loadAnimation(this, R.anim.together_animation);
        cv_logo.clearAnimation();
        cv_logo.startAnimation(translate);

        translate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (globalPreferences.getLoginStatus()) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                 else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_right);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setLocale(String lang) {
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(globalPreferences.getLang())); // API 17+ only.
        res.updateConfiguration(conf, dm);
    }

}
