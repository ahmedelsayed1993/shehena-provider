package com.aait.shehenaprovider.App;

import android.app.Application;

import com.aait.shehenaprovider.R;
import com.aait.shehenaprovider.Utils.GlobalPreferences;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;



public class AppController extends Application {
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;



    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;
    public static GlobalPreferences globalPreferences;


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/JF-Flat-regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }


}