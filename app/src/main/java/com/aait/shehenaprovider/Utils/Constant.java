package com.aait.shehenaprovider.Utils;


public class Constant {
    //GPS
    public static int GPSEnabling = 300;



    public static final String SEARCH = "searchquery";

    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String ID = "id";
    public static final String SCANMODEL = "scan_model";
    public static final String LOCATION = "location";
    public static final String SELECTEDITEM = "selected_item";


    public static final class PermissionCode {
        public static final int STORAGE = 1;
        public static final int CAMERA = 8;

    }

    public static final class RequestCode {
        public static int GETLOCATION = 500;
        public static final int PHOTO_CHOOSE = 3;
        public static final int GPSEnabling = 300;
        public static final int Call = 100;
        public static final int Take_PICTURE = 9;
        public static final int mA = 1;
        public static final int mB = 2;
        public static final int mC = 4;




    }

}
